# Custom Report Documentation

This is a guide for setting up a local LeanIX development environment and publishing Custom Reports to your LeanIX workspace.

# Setup

NOTE: If you run into any errors following the below steps, try looking up the error on Stack Overflow/Github Support for a quick fix!

## Applications to install
- Visual Studio: Source-Code editor. You can download Visual Studio Code [here](https://code.visualstudio.com/download).
- Git 2: Used for source control and integration with GitLab. Find the version to download for your OS [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
- Node 14: Used to run various node package manager (npm) commands in the project. Find the version to download for your OS [here](https://nodejs.org/en/).

## Administrative Access

You will need administrative access to run scripts on your local Windows laptop. Please contact your administrator to acquire the required permissions.


## Getting Started With your own custom Report Project

- Go to Visual Studio and open a new terminal from the menu. Install the LeanIX client by running the command `npm install -g @leanix/reporting-cli`.
- Navigate to the directory where you want to put your LeanIX Custom Report project. For more information on how to navigate directories using the command-line click [here](https://linuxcommand.org/lc3_lts0020.php).
- Initialize a new project by running the following commands:
```
mkdir projectname
cd projectname
lxr init
npm install
```
- Enter the required information if prompted and open the newly initialized project in Visual Studio.
- Edit your lxr.json file as follows:
```
{
  "host": "us-2.leanix.net",
  "apitoken": ""
}
```
- Create a new API token from your LeanIX workspace. For information on how to create an API token click [here](https://docs.leanix.net/docs/create-and-manage-your-own-api-tokens). Populate the lxr.json file with the newly created API token.
- Launch the local development server by running the command `npm start` in the terminal.
- Congrats! You have now successfully setup the required environment for creating LeanIX Custom Reports.

## Build a Custom Report from a Boilerplate Source Code (Using this Repo)
- Go to Visual Studio and open a new terminal from the menu. Navigate to the directory where you want to put your project and clone this repo by running the following command `git clone https://gitlab.com/vanshmago/custom-reports.git`
- Navigate to either a Vanilla Javascript Report or a Vue JS Report under "Sample Reports". Here you will find the boilerplate code for your report under the "src" folder. 
- Install the required dependencies by running the following script:
```
npm install -g @leanix/reporting-cli
npm install --dev @babel/plugin-transform-runtime postcss-loader tailwindcss
npm install alpinejs
npm install
``` 
- If your report has an "Export To Excel" button, run the command `npm install exceljs file-saver`
- Launch the local development server by running the command `npm start` for a Vanilla Javascript Report and `npm run serve` for a Vue JS report.
- Edit the code to suit your requirements.

# Become a Publisher

- Go to your LeanIX workspace, click on your profile in the upper-right corner and navigate to the LeanIX store.
- On the store, navigate to the "Publish" page and scroll down to register as a Publisher.
- Upon approval of your request, navigate to "My Dashboard" to publish custom reports to the LeanIX store!

NOTE: LeanIX requires a content-provider agreement and a GitLab account to register a user as a Publisher. The content-provider agreement has already been provided; however, you still need to make a GitLab account and link it to your LeanIX workspace to initalize remote servers on GitLab for the projects you work on.

# Developing Custom Reports

- LeanIX provides step-by-step tutorials on how to build custom reports tailored to your needs. The tutorials cover how to extract data from LeanIX using the GraphQL API, transform the returned JSON response and present the transformed data using tables and visualizations.
- To access the tutorials, click [here](https://dev.leanix.net/docs/custom-report-querying-data).
- To build custom reports, you need a robust understanding of Javascript and an intermediate understanding of HTML, CSS and a minimal Javascript framework called Alpine JS. Please see the tutorials below to get started. Happy Coding!

# Custom Report Approval

- Congrats! You have created your first custom report! In order to publish your report, navigate to "My Dashboard" on the LeanIX store and click on "Reports".
- Create a new report by clicking on the + icon and get a GitLab link for your project.
- On the next page, set visibility to "Private" and enter the following workspace id: `e0f3abc0-a178-4cd1-a488-07a0fe8c3f69`.
- Fill out the rest of the information and click Save.
- Push your locally developed project to the remote server on GitLab. For information on how to use git on the command-line, see the tutorials below.
- Once your project files are on GitLab, submit your report for review on the LeanIX store.
- The LeanIX team will then review your code and suggest changes, if any. Implement the necessary changes and push the final code to the remote server.
- Once approved, the report will be published to the LeanIX store.

# Approval Hack 
- The approval process of Custom Reports might be lengthy and you might require your reports to be published urgently. For these cases, you can skip the approval process and publish the report to your environment with the following steps.
- The report will be uploaded to the workspace which was used to create the API_TOKEN. In your lxr.json file, replace the API_TOKEN accordingly.
- If you're uploading an updated report, increase the verion number in "package.json". The previous report will not be overwritten if the version number is not increased.
- In the terminal, run the following command to publish the report: `npm run upload`.
- The report will be published to the your LeanIX workspace.
- Go to your Dashboard -> Administration -> Reports to view your published report.
- In the report menu, update the value to "Custom" to make the report available to everyone in the workspace.
- Access your final report under "Reports" in the menu bar.

For the regular approval process, follow the steps below.

# Pushing the Custom Report to your workspace

- Once published to the store, you will be able to view the report under "My Private Extensions".
- Open the report and click on "Add to Workspace" or choose "Add to Multiple Workspaces" to add the report to the desired workspace.
- Go to your Dashboard -> Administration -> Reports to view your published report.
- In the report menu, update the value to "Custom" to make the report available to everyone in the workspace.
- Access your final report under "Reports" in the menu bar.

# Tutorials
1. [HTML](https://www.w3schools.com/html/) 
2. [JavaScript](https://www.w3schools.com/js/) 
3. [CSS](https://www.w3schools.com/css/) 
4. [AlpineJS](https://daily.dev/blog/alpine-js-the-ultimate-guide#x-htmlbr) 
5. [Git Commands](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html) 
6. [Git](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
