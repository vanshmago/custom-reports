// Production Token : mu22OYatDdgeAGnOAjwSBs8dHt9ytC8dTYZjwRBr
// Sandbox token: qnY8AWkqYJMdzU6nA7LPE5TcbR2D4g3q6LyyCghW
//importing alpine js, a javascript framework, to implement extended functionality such as interlinking html and js efficiently
import "alpinejs";
import "@leanix/reporting";
import "./assets/tailwind.css";
import Excel from "exceljs";
import { saveAs } from "file-saver";

// API token for respective workspace, change token here to switch between workspaces
const API_TOKEN = "qnY8AWkqYJMdzU6nA7LPE5TcbR2D4g3q6LyyCghW";

const state = {
  // variable to hold the graphql query response
  response: {
    data: {
      allTags: {
        edges: [
          {
            node: {
              name: "TMB",
              factsheets: {
                totalCount: 98,
                edges: [
                  {
                    node: {
                      displayName: "Abstract Books Database",
                      updatedAt: "2021-02-18T16:13:49.812371Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "AEM Document Data Interface Service",
                      updatedAt: "2020-12-17T15:54:22.391275Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "AEM Notification Service",
                      updatedAt: "2020-12-17T15:54:35.126258Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "AEM Workflow Service",
                      updatedAt: "2020-12-17T15:54:11.240549Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ArchiveBackendScans",
                      updatedAt: "2021-05-20T02:22:24.645716Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ArchiveOldReports",
                      updatedAt: "2021-05-20T02:22:24.631272Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Assessment File System",
                      updatedAt: "2021-02-18T18:08:17.454927Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "AUTH Product",
                      updatedAt: "2020-12-16T19:28:36.126030Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "AUTH Product / Self Service Password Reset (SSPR) / AUTH Site Backend",
                      updatedAt: "2021-02-18T18:16:49.545109Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "AUTH Product / Self Service Password Reset (SSPR) / AUTH Site UI",
                      updatedAt: "2020-12-16T14:55:04.613273Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "AutoReconciliation Service",
                      updatedAt: "2021-02-18T18:08:31.523971Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "BulkArchiveSmoosher",
                      updatedAt: "2021-05-20T02:22:24.640866Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ClamAV Service",
                      updatedAt: "2021-02-04T17:57:04.184432Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Daily Revenue Entry",
                      updatedAt: "2021-02-18T18:10:53.257995Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Document Data Interface(DDI)",
                      updatedAt: "2021-02-04T17:57:07.353725Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "EFT File Creation",
                      updatedAt: "2021-02-18T18:10:55.792448Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "EFT File Creation Service",
                      updatedAt: "2021-02-18T18:10:59.731584Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "EREG Product",
                      updatedAt: "2020-12-16T19:27:41.878640Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "EREG Product / eRegistration (eReg) Backend",
                      updatedAt: "2021-02-18T18:17:35.951106Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "EREG Product / eRegistration (eReg) UI",
                      updatedAt: "2020-12-16T14:53:29.454232Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eWorkflow",
                      updatedAt: "2021-02-04T17:57:13.848891Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "File Server (PDF/TIFF/EFT File Repository)",
                      updatedAt: "2021-02-04T17:57:15.210051Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FileStream",
                      updatedAt: "2021-02-18T18:09:38.218830Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Internal SSL Certificate Authority",
                      updatedAt: "2021-02-04T17:57:17.962398Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "IOService",
                      updatedAt: "2021-02-18T18:11:24.380533Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Knowledge Base",
                      updatedAt: "2021-02-18T18:10:35.408792Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Land Titles Database",
                      updatedAt: "2021-02-18T16:14:47.443811Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LDAP Database",
                      updatedAt: "2021-02-04T17:57:24.150712Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LTO Product",
                      updatedAt: "2021-05-10T08:37:27.459893Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "LTO Product / Land Titles Online (LTO) Backend",
                      updatedAt: "2021-05-10T23:47:03.142183Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LTO Product / Land Titles Online (LTO) UI",
                      updatedAt: "2020-12-16T14:49:25.746915Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Mail Service",
                      updatedAt: "2021-02-18T18:10:38.217458Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "MLTS",
                      updatedAt: "2021-02-04T17:57:34.436190Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Notification Service",
                      updatedAt: "2021-05-20T02:22:24.664088Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Order Service",
                      updatedAt: "2021-02-18T18:10:44.320078Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PDFFlatteningService",
                      updatedAt: "2021-02-18T18:10:48.667614Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PDS Product",
                      updatedAt: "2020-12-16T19:27:22.690535Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PDS Product / Plan Deposit Submission (PDS)  Backend",
                      updatedAt: "2021-02-18T18:17:12.485171Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PDS Product / Plan Deposit Submission (PDS) UI",
                      updatedAt: "2020-12-16T14:42:24.491759Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PPR Batch",
                      updatedAt: "2021-02-04T17:57:42.716330Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PPR Database",
                      updatedAt: "2021-02-04T17:57:44.381126Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PPR Product",
                      updatedAt: "2020-12-16T19:26:27.285867Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PPR Product / Personal Property Registry (PPR) Backend",
                      updatedAt: "2021-02-18T18:19:38.429268Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PPR Product / Personal Property Registry (PPR) UI",
                      updatedAt: "2021-02-18T17:13:50.081779Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PPR Product / Personal Property Registry (PPR) UI / ReDistribition",
                      updatedAt: "2021-02-18T17:13:50.079304Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Print Queues",
                      updatedAt: "2021-02-04T17:57:45.634065Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Printed Output",
                      updatedAt: "2021-02-18T18:11:26.935809Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PromoteReports",
                      updatedAt: "2021-05-20T02:22:24.604570Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RBC FTP Maintenance Task",
                      updatedAt: "2021-05-20T02:22:24.617981Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registration Support service",
                      updatedAt: "2021-02-18T18:11:36.111324Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Relay Email/Fax Server",
                      updatedAt: "2021-02-04T17:57:57.674133Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Remove LiveCycle Originals",
                      updatedAt: "2021-05-20T02:22:24.527548Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Reset WRS",
                      updatedAt: "2021-02-18T18:11:42.122122Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RMS Database",
                      updatedAt: "2021-02-18T16:15:14.248594Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Scheduled FTP Service",
                      updatedAt: "2021-02-18T18:11:44.973051Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Scheduled Print Service",
                      updatedAt: "2021-02-18T18:12:18.147558Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Scheduled Report Service",
                      updatedAt: "2021-02-18T18:13:43.400013Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Smart Forms Transfer (T), Mortgage (M), Caveat (C), Discharge (D)",
                      updatedAt: "2021-02-04T17:58:14.199935Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SMTP2Go Cloud",
                      updatedAt: "2021-02-04T17:58:15.317166Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SQL Reporting System (SSRS)",
                      updatedAt: "2021-02-04T17:58:15.947069Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB)",
                      updatedAt: "2021-02-18T18:14:26.594504Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Digitization",
                      updatedAt: "2021-02-18T18:12:31.498276Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Knowledge Base",
                      updatedAt: "2021-02-18T18:12:34.612474Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Land Titles",
                      updatedAt: "2021-02-18T18:12:37.407824Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Survey",
                      updatedAt: "2021-02-18T18:12:40.528621Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Task Manager",
                      updatedAt: "2021-02-18T18:12:44.265082Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books Database",
                      updatedAt: "2021-02-18T16:15:31.810153Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books Update Document Service",
                      updatedAt: "2021-02-18T18:12:47.901397Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SurveyBooks Documents Service",
                      updatedAt: "2021-02-18T18:12:51.214470Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Tax Office Database (TOD)",
                      updatedAt: "2021-02-08T15:03:59.088439Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TCHK Product",
                      updatedAt: "2020-12-16T19:26:45.345882Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TCHK Product / Title Check (TCHK) Backend",
                      updatedAt: "2021-02-18T18:18:59.408516Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TCHK Product / Title Check (TCHK) UI",
                      updatedAt: "2021-02-09T15:25:45.428250Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "tmb-core-ui-addon 3.12.19",
                      updatedAt: "2020-12-14T21:47:44.334088Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR AD",
                      updatedAt: "2021-02-04T17:58:37.814527Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client",
                      updatedAt: "2021-02-18T18:12:54.382362Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "TPR Client / Advanced Account Management System (AAMS) Module",
                      updatedAt: "2021-02-18T18:12:57.470923Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client / Apps",
                      updatedAt: "2021-02-18T18:13:01.336307Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client / Document Tracking",
                      updatedAt: "2021-02-18T18:13:04.418109Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client / Find Sales",
                      updatedAt: "2021-02-18T18:13:08.419587Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "TPR Client / Firms, Users, Security Profile, Settings",
                      updatedAt: "2021-02-18T18:13:11.775424Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "TPR Client / Manitoba Land Titles System (MLTS) Module",
                      updatedAt: "2021-02-18T18:13:15.057242Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client / Reports",
                      updatedAt: "2021-02-18T18:13:17.861729Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Database",
                      updatedAt: "2021-02-18T16:16:01.225101Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Exports",
                      updatedAt: "2021-02-18T18:13:20.649116Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR PDF Splitter service",
                      updatedAt: "2021-02-18T18:13:23.785570Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Reports/TPR Exports/Update XLSX",
                      updatedAt: "2021-02-18T18:13:27.007555Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Scan and Trigger service",
                      updatedAt: "2021-02-18T18:13:29.656243Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Service",
                      updatedAt: "2021-02-18T18:13:33.593856Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Web SiteTax Office Database (TOD)",
                      updatedAt: "2021-02-04T17:58:53.166584Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Workflow Document service",
                      updatedAt: "2021-02-18T18:13:51.169768Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "TPR's Unified Data Repository Administration (TUNDRA) Module",
                      updatedAt: "2021-02-18T18:14:03.377440Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "tprmb.ca Web Site",
                      updatedAt: "2021-02-04T17:58:57.509874Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Transaction Batch Acceptance Rejection (TBAR)",
                      updatedAt: "2021-02-18T18:14:30.090633Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "UMT - User Management System",
                      updatedAt: "2021-02-04T17:59:00.344501Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Workflow Initialization Service",
                      updatedAt: "2021-05-20T02:22:24.583679Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Workflow Registration Service",
                      updatedAt: "2021-05-20T02:22:24.558769Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "WorkflowWrapperService",
                      updatedAt: "2021-02-18T18:14:40.518414Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Security",
              factsheets: {
                totalCount: 18,
                edges: [
                  {
                    node: {
                      displayName: "Cofense PhishMe Cloud",
                      updatedAt: "2020-07-03T15:08:53.714144Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Digital Guardian DLP Cloud",
                      updatedAt: "2020-07-03T15:09:54.308154Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "F5 Networks BIG-IP Application Security Manager (ASm)",
                      updatedAt: "2020-06-17T16:06:11.969582Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "F5 Networks Silverline DDoS Cloud",
                      updatedAt: "2020-07-03T15:10:45.071551Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Gemalto SafeNet HSM Client",
                      updatedAt: "2020-05-14T04:29:29.961225Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Imperva SecureSphere",
                      updatedAt: "2020-07-21T03:07:09.344542Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Azure Active Directory Cloud",
                      updatedAt: "2020-07-03T18:34:41.063940Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Nessus Security Center",
                      updatedAt: "2020-07-08T18:36:00.007365Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Okta Identity Cloud",
                      updatedAt: "2020-07-03T15:12:37.800489Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OpenLDAP",
                      updatedAt: "2020-07-08T18:40:34.326809Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Prisma Cloud Compute",
                      updatedAt: "2020-11-03T20:39:09.776257Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Private Ark Password Vault",
                      updatedAt: "2020-07-08T18:39:54.598561Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Rapid7 Suit Cloud",
                      updatedAt: "2020-06-17T03:49:26.291207Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RSA SecurID Authentication",
                      updatedAt: "2020-12-16T15:16:15.910434Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SANS Security Awareness Cloud",
                      updatedAt: "2020-07-03T15:15:01.894414Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Symantec Antivirus",
                      updatedAt: "2020-07-21T03:08:24.548592Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Symantec SEP",
                      updatedAt: "2020-06-04T14:23:37.050322Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Tripwire",
                      updatedAt: "2020-12-16T15:22:07.976169Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "ENT SYS",
              factsheets: {
                totalCount: 30,
                edges: [
                  {
                    node: {
                      displayName: "Box.com Cloud",
                      updatedAt: "2020-11-19T20:12:49.578689Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CDC Framework",
                      updatedAt: "2020-06-18T12:43:02.342585Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CMS Registries Operational Dashboard",
                      updatedAt: "2020-08-12T19:38:36.650022Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Cobblestone Contract Insight 7 (7.0)",
                      updatedAt: "2020-07-06T13:21:18.395588Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Data Lake",
                      updatedAt: "2020-05-14T03:44:14.113184Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "DocuSign Cloud",
                      updatedAt: "2020-06-09T15:16:19.527353Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ESR - Mapping KPI",
                      updatedAt: "2020-05-14T03:44:14.211102Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ESR - Performance Reporting",
                      updatedAt: "2020-05-14T03:44:14.315322Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - Commercial Revenue Report",
                      updatedAt: "2020-05-14T03:44:14.421984Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - Financial Business Forecasting",
                      updatedAt: "2020-08-12T19:34:21.411259Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - MOR Dashboard",
                      updatedAt: "2020-12-09T00:15:07.403915Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - Registration & Search Model",
                      updatedAt: "2020-05-14T03:44:14.640815Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - Weekly Volume Report",
                      updatedAt: "2020-05-14T03:44:14.738284Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FileNet ECM",
                      updatedAt: "2020-06-09T15:20:49.848926Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Kainos Smart Cloud",
                      updatedAt: "2020-12-09T16:41:28.296104Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Pluralsight Cloud",
                      updatedAt: "2020-06-29T16:14:20.996300Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SAP BPC",
                      updatedAt: "2020-06-10T17:44:13.285419Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SAP Business Objects",
                      updatedAt: "2020-06-05T21:34:41.020777Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SAP BW",
                      updatedAt: "2020-06-10T17:46:00.752337Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SAP ECC",
                      updatedAt: "2020-06-10T17:46:59.519047Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ServiceNow Cloud",
                      updatedAt: "2020-12-08T18:46:18.903571Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SharePoint",
                      updatedAt: "2020-10-05T14:51:42.320715Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Tableau",
                      updatedAt: "2020-12-15T17:05:33.176722Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS  AVM 2",
                      updatedAt: "2020-05-14T03:44:14.928688Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS CHMC Fraud Detection",
                      updatedAt: "2020-05-14T03:44:15.054217Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS OMI",
                      updatedAt: "2020-05-14T03:44:15.144727Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS Teranet HPI",
                      updatedAt: "2020-08-12T19:45:35.373526Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS Teranet Insight Report",
                      updatedAt: "2020-08-12T19:44:42.336298Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Workday Cloud",
                      updatedAt: "2020-12-08T18:43:47.084267Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Zendesk Cloud",
                      updatedAt: "2020-06-11T20:31:45.992583Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "SDLC SYS",
              factsheets: {
                totalCount: 22,
                edges: [
                  {
                    node: {
                      displayName: "Aha! Cloud",
                      updatedAt: "2020-07-13T15:09:07.387905Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ALMQC",
                      updatedAt: "2020-06-17T13:32:40.780457Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Artifactory",
                      updatedAt: "2020-07-13T18:16:48.479737Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Atlassian Bitbucket",
                      updatedAt: "2020-12-11T19:20:33.540143Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Atlassian Confluence",
                      updatedAt: "2020-12-11T19:12:43.871277Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Atlassian JIRA",
                      updatedAt: "2020-12-11T19:24:39.630880Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Beyond Compare",
                      updatedAt: "2020-12-11T20:39:42.076515Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Blueprint Storyteller",
                      updatedAt: "2020-06-17T13:42:25.589832Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ConnectALL Integration Platform",
                      updatedAt: "2020-06-17T13:34:30.986197Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Dependency Checker",
                      updatedAt: "2020-05-14T04:15:27.678167Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Gerrit",
                      updatedAt: "2020-06-17T13:12:50.500673Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Git",
                      updatedAt: "2020-06-17T13:20:23.570021Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "JAWS",
                      updatedAt: "2020-05-14T04:15:28.236943Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Jenkins CI",
                      updatedAt: "2020-07-13T18:22:21.067959Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Micro Focus Fortify",
                      updatedAt: "2020-07-20T17:59:48.710559Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OWASP ZAP",
                      updatedAt: "2020-05-14T04:15:28.827986Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Resharper Ultimate",
                      updatedAt: "2020-12-11T20:53:16.253974Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Selenium Framework",
                      updatedAt: "2021-02-18T18:23:52.239288Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SmartBear ReadyAPI!",
                      updatedAt: "2020-05-14T04:15:29.415685Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SmartGit",
                      updatedAt: "2020-06-17T13:25:34.847906Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Sparx Enterprise Architect",
                      updatedAt: "2020-05-14T04:15:30.007601Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "XMLSpy",
                      updatedAt: "2020-05-14T04:15:30.296079Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "INFRASTRUCTURE",
              factsheets: {
                totalCount: 20,
                edges: [
                  {
                    node: {
                      displayName: "Active Directory",
                      updatedAt: "2021-02-04T17:51:47.076393Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Bradmark Surveillance",
                      updatedAt: "2020-07-08T18:05:32.432648Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Cisco AppDynamics Cloud",
                      updatedAt: "2020-07-03T14:14:25.644962Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Cisco Intersight Cloud",
                      updatedAt: "2020-07-28T14:09:45.728745Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Cisco Systems Cloud Security Broker Cloud",
                      updatedAt: "2020-07-03T13:43:15.921221Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Elastic Stack",
                      updatedAt: "2020-07-08T18:22:04.289156Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "F5 Networks BIG-IP Application Security Manager (ASm)",
                      updatedAt: "2020-06-17T16:06:11.969582Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "HCL BigFix",
                      updatedAt: "2020-12-08T15:22:59.753053Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "IBM DB2",
                      updatedAt: "2020-12-08T15:20:37.059748Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "IBM VoiceTrust",
                      updatedAt: "2020-07-07T16:00:19.607381Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Active Directory 2012",
                      updatedAt: "2020-07-03T18:37:08.638541Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Office 365 Enterprise",
                      updatedAt: "2020-08-23T16:30:02.607691Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft SCCM",
                      updatedAt: "2020-07-14T19:10:28.387366Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Skype for Business",
                      updatedAt: "2020-07-08T18:33:56.039664Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft SQL Server",
                      updatedAt: "2020-12-11T18:45:55.215555Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Teams",
                      updatedAt: "2020-07-14T19:14:00.022953Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Oracle Database and DBMS",
                      updatedAt: "2020-06-17T16:58:40.611608Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PostgreSQL",
                      updatedAt: "2020-06-17T16:43:46.323542Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SolarWinds Orion",
                      updatedAt: "2020-07-14T13:28:18.071075Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Zoom Cloud",
                      updatedAt: "2020-08-23T16:31:07.368212Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "COMM SOLUTIONS",
              factsheets: {
                totalCount: 57,
                edges: [
                  {
                    node: {
                      displayName: "Act-ON",
                      updatedAt: "2020-11-19T20:12:03.418583Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CommHub",
                      updatedAt: "2020-11-19T18:13:52.551174Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Condo Concentration",
                      updatedAt: "2020-11-19T18:14:06.399333Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Conveyancer",
                      updatedAt: "2020-11-19T18:17:59.553973Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CPS",
                      updatedAt: "2020-06-18T00:33:41.309795Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Do Process Wrapper",
                      updatedAt: "2020-06-29T15:58:04.463805Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "E-ComUnity",
                      updatedAt: "2020-06-29T15:59:14.766188Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Estate-A-Base",
                      updatedAt: "2020-11-19T20:13:12.759176Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eStrataHub",
                      updatedAt: "2020-11-19T18:20:22.308448Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FastCompany",
                      updatedAt: "2020-11-19T20:04:20.655894Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse",
                      updatedAt: "2020-11-19T20:02:29.112355Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / eStore",
                      updatedAt: "2020-11-19T18:14:40.893143Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse Api",
                      updatedAt: "2020-11-19T20:08:11.863271Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse CustomerCare",
                      updatedAt: "2020-11-19T20:09:16.544323Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse DB",
                      updatedAt: "2020-12-08T15:06:02.423050Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Geowarehouse / Geowarehouse Management Report",
                      updatedAt: "2020-11-19T20:13:26.938593Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse Product Admin",
                      updatedAt: "2020-11-19T20:13:42.740833Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse Reporting",
                      updatedAt: "2020-11-19T20:08:43.148940Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse UI",
                      updatedAt: "2020-11-19T20:14:32.476144Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / HeatMaps",
                      updatedAt: "2020-11-19T20:17:38.728683Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Map Tile Service",
                      updatedAt: "2020-11-19T20:14:13.090856Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Geowarehouse / Map Tile Service / Geomedia WebMap",
                      updatedAt: "2020-11-19T20:18:06.166327Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Google Analytics",
                      updatedAt: "2020-11-19T20:42:12.243645Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Hosted Pay Page",
                      updatedAt: "2020-11-19T20:43:30.437891Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LendView",
                      updatedAt: "2020-12-09T00:14:44.634529Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Paytickets",
                      updatedAt: "2020-06-29T16:10:12.664249Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Pin Monitoring",
                      updatedAt: "2020-11-19T20:45:07.636259Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ProSuite",
                      updatedAt: "2020-11-19T20:45:46.023720Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview",
                      updatedAt: "2020-12-10T19:47:43.859262Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / DB",
                      updatedAt: "2020-11-19T20:46:28.003658Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / Purview Application",
                      updatedAt: "2020-11-19T20:46:45.088114Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / Purview Reporting",
                      updatedAt: "2020-11-19T20:47:24.120169Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / Reavs",
                      updatedAt: "2020-11-19T21:26:51.157534Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / Reavs / Reavs DB",
                      updatedAt: "2020-11-19T20:47:39.481504Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RedX",
                      updatedAt: "2020-11-19T21:29:01.072519Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RedX / RedX DB",
                      updatedAt: "2020-11-19T21:29:18.414797Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RedX / RedX Desktop",
                      updatedAt: "2020-11-19T21:29:33.183518Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RedX / RedX Web",
                      updatedAt: "2020-11-19T21:29:48.071219Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Sprout Social Cloud",
                      updatedAt: "2020-08-13T21:25:42.453999Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TCOL",
                      updatedAt: "2020-11-19T21:30:56.701534Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange",
                      updatedAt: "2020-11-19T21:31:33.656803Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / Teranet Xchange Batch",
                      updatedAt: "2020-11-19T21:31:47.335035Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / Teranet Xchange DB",
                      updatedAt: "2020-11-19T21:32:05.044232Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / Teranet Xchange Online",
                      updatedAt: "2020-11-19T21:32:22.327608Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / Teranet Xchange Web",
                      updatedAt: "2020-11-19T21:32:55.850371Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / TeranetXchange OnDemand",
                      updatedAt: "2020-11-19T21:33:06.877106Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / TeranetXchange TCopy",
                      updatedAt: "2020-11-19T21:33:36.672149Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TSOL",
                      updatedAt: "2020-11-19T21:36:24.566562Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Billing",
                      updatedAt: "2020-12-08T15:28:11.945336Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law",
                      updatedAt: "2020-11-19T21:36:04.810425Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Docgen",
                      updatedAt: "2020-11-19T21:37:27.440947Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity Law API",
                      updatedAt: "2020-12-10T14:20:28.789897Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity Law Connect",
                      updatedAt: "2020-11-19T21:37:39.900534Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity Law DB",
                      updatedAt: "2020-11-19T21:37:51.183418Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity Law Web",
                      updatedAt: "2020-11-19T21:38:05.882129Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity PI",
                      updatedAt: "2020-11-19T21:38:21.219318Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "WillBuilder",
                      updatedAt: "2020-11-19T21:38:53.452986Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "ESR",
              factsheets: {
                totalCount: 129,
                edges: [
                  {
                    node: {
                      displayName: "CDS",
                      updatedAt: "2020-06-19T16:44:22.452465Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CMV",
                      updatedAt: "2020-06-19T11:51:53.215464Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CRS",
                      updatedAt: "2020-11-27T21:41:51.030682Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CSP Framework  ",
                      updatedAt: "2020-06-19T16:57:45.348464Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CSP Framework   / CSP DB  ",
                      updatedAt: "2020-06-06T14:23:32.867699Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Elastic Search Service",
                      updatedAt: "2020-06-19T11:53:18.178671Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap",
                      updatedAt: "2020-06-05T18:43:36.076373Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Copycache",
                      updatedAt: "2020-06-11T13:06:01.622402Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Daily Update",
                      updatedAt: "2020-06-15T14:42:51.778014Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / eMap DB",
                      updatedAt: "2020-06-01T17:40:55.779407Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Health Check",
                      updatedAt: "2020-06-01T17:32:27.833694Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Log Analyzer",
                      updatedAt: "2020-06-11T13:06:32.327506Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Log Archiving",
                      updatedAt: "2020-06-01T17:33:40.751794Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Log Monitor",
                      updatedAt: "2020-06-05T19:13:35.125841Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / PIN Notification",
                      updatedAt: "2020-06-11T13:07:05.074061Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Server & Customizations",
                      updatedAt: "2020-06-15T14:13:36.229052Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Service Publisher",
                      updatedAt: "2020-06-11T13:07:31.625813Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LRO Admin  ",
                      updatedAt: "2020-06-05T19:36:49.597983Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "MOF Data Collection  ",
                      updatedAt: "2020-06-08T20:29:28.963071Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "New TSB",
                      updatedAt: "2021-07-07T14:57:37.038242Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "New TSB Cloud",
                      updatedAt: "2021-07-07T14:58:00.382289Z",
                      qualitySeal: "APPROVED",
                    },
                  },
                  {
                    node: {
                      displayName: "OnLand   ",
                      updatedAt: "2020-06-19T16:56:53.025018Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnLand    / Onland API  ",
                      updatedAt: "2020-06-05T19:47:01.175420Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnLand    / Onland CMS  ",
                      updatedAt: "2020-06-05T19:52:54.751704Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnLand    / Onland PT  ",
                      updatedAt: "2020-06-05T20:27:47.636018Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap",
                      updatedAt: "2020-06-29T18:16:21.718167Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / AMMR - Automated Map Maintenance Request",
                      updatedAt: "2020-06-11T13:01:04.908183Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / AMMR DB",
                      updatedAt: "2020-05-29T19:05:01.746893Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / CDS Library",
                      updatedAt: "2020-06-01T17:42:39.176413Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Control Panel",
                      updatedAt: "2020-06-05T19:27:43.139259Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Desktop & Addins",
                      updatedAt: "2020-06-11T13:12:57.313680Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Enterprise Geospatial",
                      updatedAt: "2020-12-10T20:10:03.253415Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / Enterprise Geospatial / Delivery Manager",
                      updatedAt: "2020-12-10T20:10:03.252346Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / Enterprise Geospatial / Delivery SDO DB",
                      updatedAt: "2020-12-10T20:10:03.248757Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / Enterprise Geospatial / Delivery ST DB",
                      updatedAt: "2020-12-10T20:10:03.240896Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Enterprise Geospatial / Dengine",
                      updatedAt: "2020-12-10T20:10:03.256500Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / Enterprise Geospatial / Tile Generation",
                      updatedAt: "2020-12-10T20:10:03.244942Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Get Image",
                      updatedAt: "2020-06-11T13:03:37.102539Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / JPS - Job Processing Service",
                      updatedAt: "2020-06-11T13:04:08.274063Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Legal Text Parser",
                      updatedAt: "2020-05-29T19:35:45.739589Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Maintenance",
                      updatedAt: "2020-11-24T14:13:14.624467Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Mapping Title Update",
                      updatedAt: "2020-06-11T13:04:57.319581Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Monitoring",
                      updatedAt: "2020-06-05T18:18:42.323459Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / MTMS - Mapping Task Management System",
                      updatedAt: "2020-05-29T19:37:52.406614Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / MTMS DB",
                      updatedAt: "2020-05-29T19:39:23.587883Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / OnMap DB",
                      updatedAt: "2020-05-29T19:46:00.704315Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / PAT - Parcel Automation Tool",
                      updatedAt: "2020-06-05T18:18:07.754Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / QMS - Queue Management System",
                      updatedAt: "2020-06-05T18:23:49.197774Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Task Manager",
                      updatedAt: "2020-06-11T13:05:28.661515Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Title Data Warehouse DB",
                      updatedAt: "2020-05-29T19:53:37.573184Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OpenLDAP .Net Library",
                      updatedAt: "2020-06-12T12:02:22.429825Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OpenLDAP Library",
                      updatedAt: "2020-07-06T18:55:41.184475Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OWL  ",
                      updatedAt: "2020-06-06T13:57:24.231199Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PINService",
                      updatedAt: "2020-06-19T12:00:53.035933Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II",
                      updatedAt: "2020-06-16T20:46:27.631257Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS BT",
                      updatedAt: "2020-06-12T14:31:31.868345Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS DB",
                      updatedAt: "2020-06-12T14:39:44.924919Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS PT",
                      updatedAt: "2020-06-12T14:24:59.106229Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS Reports",
                      updatedAt: "2020-06-17T20:22:06.763202Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS Title Update",
                      updatedAt: "2020-06-17T14:59:54.208816Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS",
                      updatedAt: "2020-06-19T16:44:22.464520Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS / RTS BT",
                      updatedAt: "2020-06-17T20:21:44.604979Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS / RTS DB",
                      updatedAt: "2020-06-16T20:42:16.166815Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS / RTS Desktop",
                      updatedAt: "2020-06-16T20:41:17.330922Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS / RTS PT",
                      updatedAt: "2020-06-16T20:20:50.276302Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Print Manager  ",
                      updatedAt: "2020-06-06T14:04:10.742765Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Reusable File Server",
                      updatedAt: "2020-06-07T15:00:21.901265Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Reusable File Server DB",
                      updatedAt: "2020-06-06T19:34:52.262394Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Rosco",
                      updatedAt: "2020-06-06T14:22:01.375632Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Rosco / Rosco PT  ",
                      updatedAt: "2020-06-19T16:52:57.424556Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Express  ",
                      updatedAt: "2020-06-08T20:26:51.143878Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Express   / Teranet Express PT  ",
                      updatedAt: "2020-06-06T15:03:53.202819Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Express   / WEF  ",
                      updatedAt: "2020-06-06T15:11:41.908462Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview",
                      updatedAt: "2021-06-23T15:26:16.182491Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Blob Repository",
                      updatedAt: "2020-11-10T22:20:37.337594Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Blob Sync",
                      updatedAt: "2020-06-16T20:33:44.258098Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Bookloading",
                      updatedAt: "2020-06-16T20:35:03.340746Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Bulk Reporting",
                      updatedAt: "2020-06-16T20:35:28.828872Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / CAS",
                      updatedAt: "2020-12-16T15:16:15.898250Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / CSC Teradmin PT",
                      updatedAt: "2020-09-30T14:08:54.451703Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Database AppBatch Jobs",
                      updatedAt: "2020-06-05T19:51:11.136904Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / DAWS",
                      updatedAt: "2021-06-23T15:26:20.682065Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / EFT",
                      updatedAt: "2020-06-16T20:37:29.849691Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / GL",
                      updatedAt: "2020-06-16T20:37:09.658339Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Image Upload",
                      updatedAt: "2020-07-28T13:59:30.402858Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / jBlobsvr",
                      updatedAt: "2020-07-14T14:54:08.198957Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / jCert",
                      updatedAt: "2020-06-16T20:36:18.403674Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Teraview / Long Term Signature Verification",
                      updatedAt: "2020-06-16T20:43:19.184349Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Lsuc/AOLS Scripts",
                      updatedAt: "2020-06-05T22:03:49.471049Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / LTTSXML",
                      updatedAt: "2020-06-19T15:35:43.317032Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Management Report",
                      updatedAt: "2020-06-05T20:59:41.236378Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / MyTeranetConnect",
                      updatedAt: "2021-06-23T15:26:24.373446Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / NGTV BRMS",
                      updatedAt: "2020-06-05T22:20:15.347897Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / NGTV BRMS DB",
                      updatedAt: "2020-06-05T22:18:02.465253Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / NGTV Portas",
                      updatedAt: "2021-02-04T19:59:12.359708Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Pass DB",
                      updatedAt: "2020-12-11T18:45:58.213691Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Password Change Scripts",
                      updatedAt: "2020-06-05T21:19:34.606928Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / PortasAdmin",
                      updatedAt: "2020-06-16T20:41:55.104565Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Teraview / Recovery - Pending Registrations",
                      updatedAt: "2020-06-05T21:25:25.936951Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Request Processor",
                      updatedAt: "2020-12-04T16:58:58.686187Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Server Monitor",
                      updatedAt: "2020-06-16T20:41:12.112878Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / TD Diamond",
                      updatedAt: "2020-06-16T20:40:54.865622Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teradmin BT",
                      updatedAt: "2020-06-16T20:40:31.154396Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Audit Archived DB",
                      updatedAt: "2020-12-11T18:46:00.599998Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Billing DB",
                      updatedAt: "2020-06-05T21:44:25.586306Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Central DB",
                      updatedAt: "2020-06-08T15:27:46.433996Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Ltts DB",
                      updatedAt: "2020-12-11T18:46:03.637012Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Report DB",
                      updatedAt: "2020-12-11T18:46:16.464448Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Support Portal",
                      updatedAt: "2020-06-06T01:17:52.809186Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Web BT",
                      updatedAt: "2020-07-28T13:58:13.195237Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Web eReg PT",
                      updatedAt: "2020-06-19T15:34:02.412305Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Web LRO PT",
                      updatedAt: "2020-06-06T01:20:56.992075Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview.ca",
                      updatedAt: "2020-06-15T16:32:59.408184Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / WEF Portas",
                      updatedAt: "2021-05-05T19:52:12.387678Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Tesim.io Cloud",
                      updatedAt: "2020-07-17T19:14:52.622442Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TSB",
                      updatedAt: "2021-07-07T14:57:36.911852Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS LendView",
                      updatedAt: "2020-08-12T19:43:53.804433Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs",
                      updatedAt: "2020-06-07T15:05:47.528752Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / New WEF",
                      updatedAt: "2020-06-07T15:13:12.670386Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / New WEF DB",
                      updatedAt: "2020-06-06T19:39:15.644006Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Remote Access Service (RAAC)",
                      updatedAt: "2020-06-07T15:02:39.770553Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / ReportGen",
                      updatedAt: "2020-06-06T19:28:33.226166Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / WDF",
                      updatedAt: "2020-06-06T19:27:59.321582Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / WDF DB",
                      updatedAt: "2020-06-06T19:33:17.090776Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs BT",
                      updatedAt: "2020-06-06T17:25:34.686215Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs DB",
                      updatedAt: "2020-12-08T15:12:28.958234Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs Desktop",
                      updatedAt: "2020-06-06T19:27:02.181003Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs NGBT",
                      updatedAt: "2020-06-07T19:45:49.333628Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs PT",
                      updatedAt: "2020-06-06T19:23:15.193952Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "CMS",
              factsheets: {
                totalCount: 249,
                edges: [
                  {
                    node: {
                      displayName: "Collateral Guard Enterprise (CGe)",
                      updatedAt: "2021-04-18T21:09:49.859305Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Collateral Guard Enterprise (CGe) / CGe CD",
                      updatedAt: "2020-06-29T19:15:27.021283Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Admin Dashboard Web",
                      updatedAt: "2020-09-11T13:22:51.707341Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD API Database",
                      updatedAt: "2020-09-11T13:23:41.987173Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Batch Console",
                      updatedAt: "2020-09-11T13:23:14.144541Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Batch Database",
                      updatedAt: "2020-09-11T13:23:53.733209Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Data Migration Process",
                      updatedAt: "2020-09-11T13:24:47.594257Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD LVS Report",
                      updatedAt: "2020-09-11T13:24:57.045276Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Middleware Dashboard",
                      updatedAt: "2020-09-08T13:09:47.554878Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD REST API",
                      updatedAt: "2020-12-11T21:28:16.802936Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD SOAP API",
                      updatedAt: "2020-09-11T13:25:14.578354Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE Database",
                      updatedAt: "2020-06-09T18:47:25.182207Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE Hangfire",
                      updatedAt: "2021-02-23T11:41:35.909623Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE REST API (Internal)",
                      updatedAt: "2021-06-16T01:02:53.505488Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE Web Services",
                      updatedAt: "2021-02-23T11:43:13.156390Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE_Archive Database",
                      updatedAt: "2020-06-09T18:50:27.766277Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE_Audit Database",
                      updatedAt: "2020-06-09T18:51:01.380138Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE_Docs Database",
                      updatedAt: "2020-06-09T18:51:39.134567Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE_Logs Database",
                      updatedAt: "2020-06-09T18:54:18.270681Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / Collateral Guard Enterprise (CGE) Web",
                      updatedAt: "2021-02-23T11:43:59.236421Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / PDF2Data Database",
                      updatedAt: "2020-11-10T16:52:35.126135Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / Pdf2DataAPI",
                      updatedAt: "2020-11-10T16:52:53.649650Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Azure DevOps Tools Cloud",
                      updatedAt: "2020-12-11T19:05:41.680751Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Ontario Main frame Terminal",
                      updatedAt: "2020-12-11T14:48:28.931829Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RDPRM",
                      updatedAt: "2020-10-19T20:03:03.981065Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery",
                      updatedAt: "2021-01-20T19:11:52.638390Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Account Maintenance Service (Recovery Services)",
                      updatedAt: "2021-04-29T03:11:00.001342Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Active_Download_vs2008",
                      updatedAt: "2021-04-29T03:10:59.995666Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AspState Database",
                      updatedAt: "2021-04-29T03:10:59.975750Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset Admin",
                      updatedAt: "2021-04-29T03:10:59.981675Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset Collection",
                      updatedAt: "2021-04-29T03:10:59.963759Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset Registry",
                      updatedAt: "2020-06-09T20:09:57.835012Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset Reporting",
                      updatedAt: "2020-06-29T19:15:13.299118Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset RMS",
                      updatedAt: "2020-06-05T02:59:50.617308Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset RMS Web Services",
                      updatedAt: "2020-06-29T19:15:13.700445Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.BankruptcyDailyWorklist",
                      updatedAt: "2021-04-21T01:43:37.877777Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.BNSInvalidPhoneLoad",
                      updatedAt: "2021-04-21T01:43:37.895464Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.BNSScorecard",
                      updatedAt: "2021-04-21T01:43:37.884677Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.CACSCloseout",
                      updatedAt: "2020-11-03T13:15:51.877070Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.DailyUserPerformances",
                      updatedAt: "2021-04-21T01:43:37.910436Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.FifthCycleBillingAutoTrigger",
                      updatedAt: "2020-06-29T19:15:16.174229Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.File.Upload.Validation",
                      updatedAt: "2020-06-29T19:15:16.636957Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.MOCBatchQ",
                      updatedAt: "2020-11-03T13:12:47.519130Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.RBCBOCLoad",
                      updatedAt: "2020-11-03T13:14:28.319954Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.RBCPreAuthorizedPayment",
                      updatedAt: "2021-04-21T01:43:37.900538Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.ShawResponseTrigger",
                      updatedAt: "2020-06-29T19:15:18.334507Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.TrendMonthly",
                      updatedAt: "2020-06-29T19:15:18.774672Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetAdmin Database",
                      updatedAt: "2020-06-05T02:44:29.947891Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetAudit Database",
                      updatedAt: "2020-06-12T20:52:54.368235Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetFax Database",
                      updatedAt: "2020-06-12T20:53:22.046871Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetFiles Database",
                      updatedAt: "2020-06-12T20:53:47.219655Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetSecurity Database",
                      updatedAt: "2020-06-12T20:54:09.896942Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ASSETWorkTrace",
                      updatedAt: "2021-04-29T03:11:00.014648Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AutoQNationalSales",
                      updatedAt: "2020-06-29T19:15:19.773126Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Bankrupt Account Setup Validation Service (Recovery Services)",
                      updatedAt: "2020-06-29T19:15:20.199379Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Bankruptcy Database",
                      updatedAt: "2020-06-09T20:12:34.528790Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Bankruptcy EXE",
                      updatedAt: "2020-06-29T19:15:20.648271Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Bankruptcy Highway",
                      updatedAt: "2020-06-29T19:15:21.091552Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BankruptcyAdmin Database",
                      updatedAt: "2020-06-05T02:44:50.343818Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BankruptcyAudit Database",
                      updatedAt: "2020-06-09T20:11:48.248623Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BH_PostBatch",
                      updatedAt: "2020-06-29T19:15:21.544505Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BHLoanNumberFix",
                      updatedAt: "2020-06-29T19:15:21.980441Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BHUserSynchronize",
                      updatedAt: "2020-06-29T19:15:22.428564Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Billing",
                      updatedAt: "2020-06-29T19:15:22.909262Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BKNoTaskScan",
                      updatedAt: "2020-06-29T19:15:23.393452Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BlackBookUpload",
                      updatedAt: "2020-06-03T22:40:17.235011Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BMO_Export_MC_File",
                      updatedAt: "2020-06-03T20:11:23.306832Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BmoDraft5FileExport",
                      updatedAt: "2020-06-03T20:12:46.710551Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BmoFileExport",
                      updatedAt: "2020-06-05T03:12:26.409994Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BmoFileImport",
                      updatedAt: "2020-06-03T20:15:46.595294Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BNS_Contingency",
                      updatedAt: "2020-06-04T01:35:22.741598Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BNSDebtManager2007App",
                      updatedAt: "2020-06-29T19:15:23.867586Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BNSPaymentApp",
                      updatedAt: "2020-06-05T03:12:43.461295Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BNSRedistributionBatch",
                      updatedAt: "2020-06-03T22:39:56.907234Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BountyHunter Database",
                      updatedAt: "2020-06-03T20:34:18.760802Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CCSBatch_GL",
                      updatedAt: "2020-06-03T20:40:34.736581Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CCSBatch_Tasks",
                      updatedAt: "2020-06-03T22:40:35.824422Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CCSBOCOverpaymentComplete",
                      updatedAt: "2020-06-04T01:35:55.623014Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CH Database",
                      updatedAt: "2020-06-05T02:59:10.741975Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CH_AccruedInterest",
                      updatedAt: "2020-06-04T01:37:34.815390Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CH_JudgmentAcrIntrst",
                      updatedAt: "2020-06-03T20:58:06.069527Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / ChAgencyAssignmentPostChargeOffCalculation",
                      updatedAt: "2020-06-29T19:15:27.487856Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CHAgencyUpload",
                      updatedAt: "2020-06-29T19:15:27.959265Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CHAGenericFileImport",
                      updatedAt: "2020-06-03T22:42:48.363324Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CHTTAdmin Database",
                      updatedAt: "2020-06-05T02:46:07.917895Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CIBC Database",
                      updatedAt: "2020-06-05T02:57:30.212166Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CIBC.Link.Security",
                      updatedAt: "2020-06-03T22:42:31.075835Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CIBC_Batch",
                      updatedAt: "2020-06-03T21:13:56.932610Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CleanListBatchApp",
                      updatedAt: "2020-06-03T22:42:13.624515Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ClientPrimeIntRate",
                      updatedAt: "2020-06-03T21:30:09.224820Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ClientReconcilationReport",
                      updatedAt: "2020-06-29T19:15:28.447007Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ClientUpdateManager Database",
                      updatedAt: "2020-06-12T20:54:45.066646Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CMS_DEV_COGNOS_102 Database",
                      updatedAt: "2020-06-12T20:55:07.521280Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Cognos",
                      updatedAt: "2020-06-29T19:15:28.923651Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Collection Highway",
                      updatedAt: "2020-06-03T22:41:55.166205Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Collection Services Api Service (Recovery Services)",
                      updatedAt: "2020-06-04T02:34:58.054597Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CopyFileUP",
                      updatedAt: "2020-06-04T01:38:13.639222Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Credit Beaureu Service (Recovery External Services)",
                      updatedAt: "2020-06-04T23:21:41.178238Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CreditBureauReportFileExport",
                      updatedAt: "2020-06-03T22:41:31.906119Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Critical.Batch",
                      updatedAt: "2020-06-03T22:41:13.237884Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Crystal Reports",
                      updatedAt: "2020-06-29T19:15:29.439099Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DBA Database",
                      updatedAt: "2020-06-12T20:55:26.933694Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DepositBatch",
                      updatedAt: "2020-06-03T22:10:52.528421Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH BlackBook Service (Recovery External Services)",
                      updatedAt: "2020-06-03T22:11:33.908556Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.AssignmentScoreFileImportAndExport",
                      updatedAt: "2021-04-22T02:50:45.776674Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.BNSDebtManagerFileImport",
                      updatedAt: "2021-04-22T02:50:45.767464Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.BNSOutputFiles",
                      updatedAt: "2021-04-22T02:50:45.748918Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DH.RecoveryServices.CACSImport",
                      updatedAt: "2021-04-22T02:50:45.787996Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.CentralizedFileImporter",
                      updatedAt: "2021-04-22T02:50:45.782449Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.DataConversionReports",
                      updatedAt: "2021-04-22T02:50:45.758683Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.DirectPaymentAutoRemit_DH",
                      updatedAt: "2021-01-21T11:48:18.449201Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.DirectPaymentPosting",
                      updatedAt: "2021-04-22T02:50:45.792287Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.Galaxy.AgencyCodeAssignment",
                      updatedAt: "2020-06-04T02:20:02.941338Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.GSLACDebtmanager",
                      updatedAt: "2020-06-04T02:21:48.002459Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.HondaFileImport",
                      updatedAt: "2020-06-04T02:25:38.179459Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.HSBCFileImport",
                      updatedAt: "2020-06-04T02:28:43.434539Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.HSBCNotesAndDataBackup",
                      updatedAt: "2020-06-04T02:31:13.151478Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RBCAutoClosure",
                      updatedAt: "2020-06-04T02:33:03.811037Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RBCDirectPaymentPosting",
                      updatedAt: "2021-04-21T01:43:37.892220Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RBCPapPosting",
                      updatedAt: "2021-04-21T01:43:37.890216Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RBCResolveFileImport",
                      updatedAt: "2021-04-21T01:43:37.908726Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.Redistribution",
                      updatedAt: "2020-06-04T02:42:05.890745Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RepoDepoInventoryExport",
                      updatedAt: "2020-06-04T02:49:55.077964Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.TDAFAssignmentFileToMDB",
                      updatedAt: "2020-06-04T02:51:54.331279Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.TDAFFileImport",
                      updatedAt: "2020-06-04T02:54:06.181912Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.TDAFLegacyAssignmentFiletoMDB",
                      updatedAt: "2020-06-04T02:55:24.341778Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DH.RecoveryServices.TS2Import",
                      updatedAt: "2020-06-04T02:57:10.689506Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Direct Payment Post File Service (Recovery Services)",
                      updatedAt: "2020-06-29T19:15:29.940834Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DirectPaymentAutoProcessBatch",
                      updatedAt: "2020-06-03T22:18:23.040427Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DirectPaymentAutoRemit",
                      updatedAt: "2020-06-03T22:19:53.156983Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Document Highway",
                      updatedAt: "2020-06-03T22:40:54.463956Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Equipment Database",
                      updatedAt: "2020-06-12T20:56:11.932709Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ExtinguishedDebt",
                      updatedAt: "2020-06-03T22:31:43.460957Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / File Transfer Service",
                      updatedAt: "2020-06-05T02:54:50.039901Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / GlobalDataStore Database",
                      updatedAt: "2020-06-12T20:56:32.426259Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / GLPost",
                      updatedAt: "2020-06-03T22:47:15.140679Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Hangfire Database",
                      updatedAt: "2020-06-05T02:37:47.010091Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / HSBC_Monthly_DataFeed",
                      updatedAt: "2020-06-03T22:53:26.242313Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ICAPRSL Database",
                      updatedAt: "2020-06-12T20:56:52.400158Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Insurance Highway",
                      updatedAt: "2020-06-03T22:59:11.986811Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Letter Service (Recovery Services)",
                      updatedAt: "2020-06-03T23:04:10.189979Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / LetterBatch",
                      updatedAt: "2020-06-03T23:02:18.731128Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / LH Database",
                      updatedAt: "2020-06-05T02:59:37.572127Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Litigation Highway",
                      updatedAt: "2020-06-03T23:12:12.793377Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Litigation Services Api Service (Recovery Services)",
                      updatedAt: "2020-06-03T23:14:14.023114Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / LogInterestRate",
                      updatedAt: "2020-06-05T03:13:00.064149Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / New_Download_vs2008",
                      updatedAt: "2020-06-03T23:41:44.553113Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / NightlyBatch.AutoXBF",
                      updatedAt: "2020-06-29T19:15:30.401776Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / NightlyBatch.SalesTaskMaintenance",
                      updatedAt: "2020-06-03T23:44:24.376368Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / NightlyBatchThunder",
                      updatedAt: "2020-06-03T23:49:39.320534Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Optimus Portal",
                      updatedAt: "2020-06-29T19:15:30.980865Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Optimus Web Services",
                      updatedAt: "2020-06-05T02:18:39.066866Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Optimus Windows Service",
                      updatedAt: "2020-06-03T23:56:27.771212Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / OutstandingAsgn",
                      updatedAt: "2020-06-04T00:02:19.192599Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Payment Highway",
                      updatedAt: "2020-06-04T00:06:12.763073Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PH_SLInvoiceMiluGenerator",
                      updatedAt: "2020-06-04T00:08:15.767058Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PPSA",
                      updatedAt: "2020-06-29T19:15:31.456701Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PPSA Database",
                      updatedAt: "2020-06-09T20:12:38.964202Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PPSAAdmin Database",
                      updatedAt: "2020-06-05T02:46:57.068045Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PrimeRateChangeLowestInterest",
                      updatedAt: "2020-06-29T19:15:31.961675Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Property Highway",
                      updatedAt: "2020-06-29T19:15:32.450113Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RBC_Intmast",
                      updatedAt: "2020-06-04T01:07:23.702470Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RBC_Redistribution",
                      updatedAt: "2020-06-04T01:08:32.423767Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RbcFileExport",
                      updatedAt: "2020-06-04T01:09:50.740401Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RBCGLBatch_Process",
                      updatedAt: "2020-06-04T01:11:02.991924Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RBCIThirdPartyLoad",
                      updatedAt: "2020-06-04T01:12:56.776549Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Rdstrbtn_CleanUP",
                      updatedAt: "2020-06-04T01:14:10.531483Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RecalcColInvPerDiem",
                      updatedAt: "2020-06-04T01:15:25.895306Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RecalcPerDiemJudgmnt",
                      updatedAt: "2020-06-04T01:17:45.112202Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Recovery Hangfire",
                      updatedAt: "2020-06-09T19:40:26.363956Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Redistribution",
                      updatedAt: "2020-06-04T17:09:58.240152Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Registries Online",
                      updatedAt: "2020-06-04T23:16:38.748105Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Repo Highway",
                      updatedAt: "2020-06-04T23:21:53.093075Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RepoDepo Database",
                      updatedAt: "2020-06-29T19:15:32.966137Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Report Viewer",
                      updatedAt: "2020-06-29T19:15:33.475604Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / ReportLog Service (Asset Audit International Services)",
                      updatedAt: "2020-06-04T23:25:03.784076Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / ReportLog Service (Asset Audit Services)",
                      updatedAt: "2020-06-04T23:58:08.447074Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ReportServer Database",
                      updatedAt: "2020-06-29T19:15:33.943696Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RevokeStatus",
                      updatedAt: "2020-06-04T23:29:45.707352Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / RH Audit Log Service (Asset Audit International Services)",
                      updatedAt: "2020-06-04T23:31:11.805305Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / RH Audit Log Service (Asset Audit Services)",
                      updatedAt: "2020-06-04T23:33:33.709541Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RHCostRecoveryGLInvoice",
                      updatedAt: "2020-06-04T23:43:29.869460Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMS Db",
                      updatedAt: "2020-06-29T19:15:34.450287Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMS2 Database",
                      updatedAt: "2020-06-29T19:15:34.965660Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMSABDemandRelocateScan",
                      updatedAt: "2020-06-04T23:50:43.036105Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMSNOTES Database",
                      updatedAt: "2020-06-29T19:15:35.462220Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMSTaskMissing",
                      updatedAt: "2020-06-04T23:52:23.321898Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMSWI Database",
                      updatedAt: "2020-06-29T19:15:35.944678Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SDALegacyLoad",
                      updatedAt: "2020-06-04T23:54:09.043504Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SetlementStatus",
                      updatedAt: "2020-06-04T23:56:01.040901Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Skip Locator",
                      updatedAt: "2020-06-04T23:59:03.139542Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Skip_NightBatch",
                      updatedAt: "2020-06-05T00:02:47.654150Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipBankReview",
                      updatedAt: "2020-06-05T02:04:00.202104Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipLocator Database",
                      updatedAt: "2020-06-29T19:15:36.461559Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipLocatorAdmin Database",
                      updatedAt: "2020-06-29T19:15:36.960154Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipLocatorDTS",
                      updatedAt: "2020-06-05T02:05:23.301845Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipNewAssignment",
                      updatedAt: "2020-06-05T02:09:32.833949Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SRQ",
                      updatedAt: "2020-06-05T02:11:41.454289Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SRQ Database",
                      updatedAt: "2020-06-29T19:15:37.467271Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SSRS Reports Server",
                      updatedAt: "2020-06-05T02:12:45.864106Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Statement Generation Service",
                      updatedAt: "2020-06-05T02:16:18.039067Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / StatementBatch",
                      updatedAt: "2020-06-05T02:17:29.101588Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / TaskService Database",
                      updatedAt: "2020-06-29T19:15:37.993253Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / TravelersAssignments",
                      updatedAt: "2020-06-05T02:18:47.408227Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / TravelersResponse",
                      updatedAt: "2020-06-05T02:19:42.582077Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / TyMetrixBatch",
                      updatedAt: "2020-06-05T02:20:36.879760Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / UserActivity Service (Asset Audit International Services)",
                      updatedAt: "2020-06-05T02:23:09.944918Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / UserActivity Service (Asset Audit Services)",
                      updatedAt: "2020-06-05T02:25:17.360926Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / WorkflowAutomation",
                      updatedAt: "2020-06-05T02:29:34.650753Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD",
                      updatedAt: "2020-06-29T19:15:26.666392Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD BAS",
                      updatedAt: "2020-06-29T19:15:23.891303Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD BBY CSRS RPA",
                      updatedAt: "2020-06-29T19:15:23.918709Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD DataLake",
                      updatedAt: "2020-06-29T19:15:23.953273Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC",
                      updatedAt: "2020-06-29T19:15:24.207954Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC Admin Web",
                      updatedAt: "2020-06-29T19:15:23.994290Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD KYC / CD KYC Automation GUI",
                      updatedAt: "2020-06-29T19:15:24.023781Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC Client Web",
                      updatedAt: "2020-06-29T19:15:24.057575Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC Database",
                      updatedAt: "2020-06-29T19:15:24.090495Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC REST API",
                      updatedAt: "2020-06-29T19:15:24.126378Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD KYC / CD KYC Service Scheduler",
                      updatedAt: "2020-06-29T19:15:24.164193Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC Web Service",
                      updatedAt: "2020-06-29T19:15:24.206016Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy BBY CSRS Interanet Web (JSP)",
                      updatedAt: "2020-06-29T19:15:24.275624Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy BBY CSRS Web (ASPX)",
                      updatedAt: "2020-06-29T19:15:24.329960Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY Database",
                      updatedAt: "2020-06-29T19:15:24.387607Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY DOSPPSA",
                      updatedAt: "2020-06-29T19:15:24.440747Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy BBY File Dispatcher",
                      updatedAt: "2020-06-29T19:15:24.501759Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY HOST",
                      updatedAt: "2020-06-29T19:15:24.565656Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY Reports",
                      updatedAt: "2020-06-29T19:15:24.632380Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY WinBill",
                      updatedAt: "2020-06-29T19:15:24.702517Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY WINPPSA",
                      updatedAt: "2020-06-29T19:15:24.786102Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy ClearCharge",
                      updatedAt: "2020-06-29T19:15:24.866788Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Admin Web",
                      updatedAt: "2020-06-29T19:15:24.941257Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Automation GUI",
                      updatedAt: "2020-06-29T19:15:25.022509Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Batch Console",
                      updatedAt: "2020-06-29T19:15:25.108930Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Client Web",
                      updatedAt: "2020-06-29T19:15:25.198578Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Database",
                      updatedAt: "2020-06-29T19:15:25.302693Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Web Service",
                      updatedAt: "2020-06-29T19:15:25.392503Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy CWI TDAF Web",
                      updatedAt: "2020-06-29T19:15:25.483827Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy CWI TDFS Web",
                      updatedAt: "2020-06-29T19:15:25.590912Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy MDA",
                      updatedAt: "2020-06-29T19:15:25.680337Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy RDPRM",
                      updatedAt: "2020-06-29T19:15:25.787786Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Streamloan Database",
                      updatedAt: "2020-06-29T19:15:25.892323Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Streamloan Document Generator",
                      updatedAt: "2020-06-29T19:15:25.997060Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy Streamloan Web",
                      updatedAt: "2020-06-29T19:15:26.102078Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD LVS SPAES Database",
                      updatedAt: "2020-09-17T15:51:39.768845Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD LVS SPAES Web Service",
                      updatedAt: "2020-09-17T15:52:10.088631Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD QMS Database",
                      updatedAt: "2020-09-17T15:52:26.586832Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD QMS Regsitry Web",
                      updatedAt: "2020-09-17T15:52:36.796445Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD QMS Universal Web",
                      updatedAt: "2020-09-17T15:52:50.041062Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "CORP IT",
              factsheets: {
                totalCount: 1,
                edges: [
                  {
                    node: {
                      displayName: "LeanIX Cloud",
                      updatedAt: "2020-07-03T18:57:33.835423Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "CORP IT",
              factsheets: {
                totalCount: 1,
                edges: [
                  {
                    node: {
                      displayName: "LeanIX Cloud",
                      updatedAt: "2020-07-03T18:57:33.835423Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Presentation Layer - Browser App",
              factsheets: {
                totalCount: 10,
                edges: [
                  {
                    node: {
                      displayName:
                        "AUTH Product / Self Service Password Reset (SSPR) / AUTH Site UI",
                      updatedAt: "2020-12-16T14:55:04.613273Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "EREG Product / eRegistration (eReg) UI",
                      updatedAt: "2020-12-16T14:53:29.454232Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse UI",
                      updatedAt: "2020-11-19T20:14:32.476144Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LTO Product / Land Titles Online (LTO) UI",
                      updatedAt: "2020-12-16T14:49:25.746915Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnLand    / Onland PT  ",
                      updatedAt: "2020-06-05T20:27:47.636018Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / QMS - Queue Management System",
                      updatedAt: "2020-06-05T18:23:49.197774Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PDS Product / Plan Deposit Submission (PDS) UI",
                      updatedAt: "2020-12-16T14:42:24.491759Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PPR Product / Personal Property Registry (PPR) UI",
                      updatedAt: "2021-02-18T17:13:50.081779Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TCHK Product / Title Check (TCHK) UI",
                      updatedAt: "2021-02-09T15:25:45.428250Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity Law Web",
                      updatedAt: "2020-11-19T21:38:05.882129Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "CORP IT",
              factsheets: {
                totalCount: 1,
                edges: [
                  {
                    node: {
                      displayName: "LeanIX Cloud",
                      updatedAt: "2020-07-03T18:57:33.835423Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Shared Code Package",
              factsheets: {
                totalCount: 21,
                edges: [
                  {
                    node: {
                      displayName: "CMV",
                      updatedAt: "2020-06-19T11:51:53.215464Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CSP Framework  ",
                      updatedAt: "2020-06-19T16:57:45.348464Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Hosted Pay Page",
                      updatedAt: "2020-11-19T20:43:30.437891Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / CDS Library",
                      updatedAt: "2020-06-01T17:42:39.176413Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OpenLDAP .Net Library",
                      updatedAt: "2020-06-12T12:02:22.429825Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OpenLDAP Library",
                      updatedAt: "2020-07-06T18:55:41.184475Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Digitization",
                      updatedAt: "2021-02-18T18:12:31.498276Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Knowledge Base",
                      updatedAt: "2021-02-18T18:12:34.612474Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Land Titles",
                      updatedAt: "2021-02-18T18:12:37.407824Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Survey",
                      updatedAt: "2021-02-18T18:12:40.528621Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Task Manager",
                      updatedAt: "2021-02-18T18:12:44.265082Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "tmb-core-ui-addon 3.12.19",
                      updatedAt: "2020-12-14T21:47:44.334088Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "TPR Client / Advanced Account Management System (AAMS) Module",
                      updatedAt: "2021-02-18T18:12:57.470923Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client / Apps",
                      updatedAt: "2021-02-18T18:13:01.336307Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client / Document Tracking",
                      updatedAt: "2021-02-18T18:13:04.418109Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client / Find Sales",
                      updatedAt: "2021-02-18T18:13:08.419587Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "TPR Client / Firms, Users, Security Profile, Settings",
                      updatedAt: "2021-02-18T18:13:11.775424Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "TPR Client / Manitoba Land Titles System (MLTS) Module",
                      updatedAt: "2021-02-18T18:13:15.057242Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client / Reports",
                      updatedAt: "2021-02-18T18:13:17.861729Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "TPR's Unified Data Repository Administration (TUNDRA) Module",
                      updatedAt: "2021-02-18T18:14:03.377440Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Billing",
                      updatedAt: "2020-12-08T15:28:11.945336Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "CORP IT",
              factsheets: {
                totalCount: 1,
                edges: [
                  {
                    node: {
                      displayName: "LeanIX Cloud",
                      updatedAt: "2020-07-03T18:57:33.835423Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "App Support Utility",
              factsheets: {
                totalCount: 7,
                edges: [
                  {
                    node: {
                      displayName: "eMap / Copycache",
                      updatedAt: "2020-06-11T13:06:01.622402Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Service Publisher",
                      updatedAt: "2020-06-11T13:07:31.625813Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse Product Admin",
                      updatedAt: "2020-11-19T20:13:42.740833Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / Enterprise Geospatial / Delivery Manager",
                      updatedAt: "2020-12-10T20:10:03.252346Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Reset WRS",
                      updatedAt: "2021-02-18T18:11:42.122122Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Support Portal",
                      updatedAt: "2020-06-06T01:17:52.809186Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "UMT - User Management System",
                      updatedAt: "2021-02-04T17:59:00.344501Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Security System",
              factsheets: {
                totalCount: 21,
                edges: [
                  {
                    node: {
                      displayName: "Active Directory",
                      updatedAt: "2021-02-04T17:51:47.076393Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Cisco Intersight Cloud",
                      updatedAt: "2020-07-28T14:09:45.728745Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Cisco Systems Cloud Security Broker Cloud",
                      updatedAt: "2020-07-03T13:43:15.921221Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Cofense PhishMe Cloud",
                      updatedAt: "2020-07-03T15:08:53.714144Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Digital Guardian DLP Cloud",
                      updatedAt: "2020-07-03T15:09:54.308154Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Entrust",
                      updatedAt: "2020-07-08T18:18:54.586172Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "F5 Networks BIG-IP Application Security Manager (ASm)",
                      updatedAt: "2020-06-17T16:06:11.969582Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "F5 Networks Silverline DDoS Cloud",
                      updatedAt: "2020-07-03T15:10:45.071551Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Gemalto SafeNet HSM Client",
                      updatedAt: "2020-05-14T04:29:29.961225Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Imperva SecureSphere",
                      updatedAt: "2020-07-21T03:07:09.344542Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Azure Active Directory Cloud",
                      updatedAt: "2020-07-03T18:34:41.063940Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Nessus Security Center",
                      updatedAt: "2020-07-08T18:36:00.007365Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Okta Identity Cloud",
                      updatedAt: "2020-07-03T15:12:37.800489Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OpenLDAP",
                      updatedAt: "2020-07-08T18:40:34.326809Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Prisma Cloud Compute",
                      updatedAt: "2020-11-03T20:39:09.776257Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Private Ark Password Vault",
                      updatedAt: "2020-07-08T18:39:54.598561Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Rapid7 Suit Cloud",
                      updatedAt: "2020-06-17T03:49:26.291207Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RSA SecurID Authentication",
                      updatedAt: "2020-12-16T15:16:15.910434Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SANS Security Awareness Cloud",
                      updatedAt: "2020-07-03T15:15:01.894414Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Symantec Antivirus",
                      updatedAt: "2020-07-21T03:08:24.548592Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Tripwire",
                      updatedAt: "2020-12-16T15:22:07.976169Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Infrastructure System",
              factsheets: {
                totalCount: 14,
                edges: [
                  {
                    node: {
                      displayName: "Bradmark Surveillance",
                      updatedAt: "2020-07-08T18:05:32.432648Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Cisco AppDynamics Cloud",
                      updatedAt: "2020-07-03T14:14:25.644962Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Elastic Stack",
                      updatedAt: "2020-07-08T18:22:04.289156Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "HCL BigFix",
                      updatedAt: "2020-12-08T15:22:59.753053Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "IBM DB2",
                      updatedAt: "2020-12-08T15:20:37.059748Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "IBM VoiceTrust",
                      updatedAt: "2020-07-07T16:00:19.607381Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Active Directory 2012",
                      updatedAt: "2020-07-03T18:37:08.638541Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft SCCM",
                      updatedAt: "2020-07-14T19:10:28.387366Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft SQL Server",
                      updatedAt: "2020-12-11T18:45:55.215555Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Oracle Database and DBMS",
                      updatedAt: "2020-06-17T16:58:40.611608Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PostgreSQL",
                      updatedAt: "2020-06-17T16:43:46.323542Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SMTP2Go Cloud",
                      updatedAt: "2021-02-04T17:58:15.317166Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SolarWinds Orion",
                      updatedAt: "2020-07-14T13:28:18.071075Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Symantec SEP",
                      updatedAt: "2020-06-04T14:23:37.050322Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Enterprise Operations System",
              factsheets: {
                totalCount: 24,
                edges: [
                  {
                    node: {
                      displayName: "Act-ON",
                      updatedAt: "2020-11-19T20:12:03.418583Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "BlackLine Unified Cloud for Accounting and Finance",
                      updatedAt: "2020-08-18T19:28:05.426845Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Box.com Cloud",
                      updatedAt: "2020-11-19T20:12:49.578689Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Cobblestone Contract Insight 7 (7.0)",
                      updatedAt: "2020-07-06T13:21:18.395588Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "DocuSign Cloud",
                      updatedAt: "2020-06-09T15:16:19.527353Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FileNet ECM",
                      updatedAt: "2020-06-09T15:20:49.848926Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Kainos Smart Cloud",
                      updatedAt: "2020-12-09T16:41:28.296104Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LeanIX Cloud",
                      updatedAt: "2020-07-03T18:57:33.835423Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Office 365 Enterprise",
                      updatedAt: "2020-08-23T16:30:02.607691Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Skype for Business",
                      updatedAt: "2020-07-08T18:33:56.039664Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Teams",
                      updatedAt: "2020-07-14T19:14:00.022953Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Pluralsight Cloud",
                      updatedAt: "2020-06-29T16:14:20.996300Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Salesforce Sales Cloud SaaS",
                      updatedAt: "2020-08-18T19:28:29.546450Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SAP BPC",
                      updatedAt: "2020-06-10T17:44:13.285419Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SAP Business Objects",
                      updatedAt: "2020-06-05T21:34:41.020777Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SAP ECC",
                      updatedAt: "2020-06-10T17:46:59.519047Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ServiceNow Cloud",
                      updatedAt: "2020-12-08T18:46:18.903571Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SharePoint",
                      updatedAt: "2020-10-05T14:51:42.320715Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Sprout Social Cloud",
                      updatedAt: "2020-08-13T21:25:42.453999Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Tableau",
                      updatedAt: "2020-12-15T17:05:33.176722Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VisionCritical Sparq",
                      updatedAt: "2020-07-03T19:02:58.141524Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Workday Cloud",
                      updatedAt: "2020-12-08T18:43:47.084267Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Zendesk Cloud",
                      updatedAt: "2020-06-11T20:31:45.992583Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Zoom Cloud",
                      updatedAt: "2020-08-23T16:31:07.368212Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "External-User Application",
              factsheets: {
                totalCount: 21,
                edges: [
                  {
                    node: {
                      displayName:
                        "AUTH Product / Self Service Password Reset (SSPR) / AUTH Site Backend",
                      updatedAt: "2021-02-18T18:16:49.545109Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "EREG Product / eRegistration (eReg) Backend",
                      updatedAt: "2021-02-18T18:17:35.951106Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / eStore",
                      updatedAt: "2020-11-19T18:14:40.893143Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LRO Admin  ",
                      updatedAt: "2020-06-05T19:36:49.597983Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "MLTS",
                      updatedAt: "2021-02-04T17:57:34.436190Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "MOF Data Collection  ",
                      updatedAt: "2020-06-08T20:29:28.963071Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OWL  ",
                      updatedAt: "2020-06-06T13:57:24.231199Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PPR Product / Personal Property Registry (PPR) Backend",
                      updatedAt: "2021-02-18T18:19:38.429268Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset Collection",
                      updatedAt: "2021-04-29T03:10:59.963759Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Insurance Highway",
                      updatedAt: "2020-06-03T22:59:11.986811Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PPSA",
                      updatedAt: "2020-06-29T19:15:31.456701Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy ClearCharge",
                      updatedAt: "2020-06-29T19:15:24.866788Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Express   / WEF  ",
                      updatedAt: "2020-06-06T15:11:41.908462Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / MyTeranetConnect",
                      updatedAt: "2021-06-23T15:26:24.373446Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / NGTV Portas",
                      updatedAt: "2021-02-04T19:59:12.359708Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Web eReg PT",
                      updatedAt: "2020-06-19T15:34:02.412305Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview.ca",
                      updatedAt: "2020-06-15T16:32:59.408184Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / WEF Portas",
                      updatedAt: "2021-05-05T19:52:12.387678Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / New WEF",
                      updatedAt: "2020-06-07T15:13:12.670386Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs Desktop",
                      updatedAt: "2020-06-06T19:27:02.181003Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs PT",
                      updatedAt: "2020-06-06T19:23:15.193952Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Application Suite/Product",
              factsheets: {
                totalCount: 41,
                edges: [
                  {
                    node: {
                      displayName: "AUTH Product",
                      updatedAt: "2020-12-16T19:28:36.126030Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Collateral Guard Enterprise (CGe)",
                      updatedAt: "2021-04-18T21:09:49.859305Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Collateral Guard Enterprise (CGe) / CGe CD",
                      updatedAt: "2020-06-29T19:15:27.021283Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Condo Concentration",
                      updatedAt: "2020-11-19T18:14:06.399333Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Conveyancer",
                      updatedAt: "2020-11-19T18:17:59.553973Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap",
                      updatedAt: "2020-06-05T18:43:36.076373Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "EREG Product",
                      updatedAt: "2020-12-16T19:27:41.878640Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Estate-A-Base",
                      updatedAt: "2020-11-19T20:13:12.759176Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eStrataHub",
                      updatedAt: "2020-11-19T18:20:22.308448Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FastCompany",
                      updatedAt: "2020-11-19T20:04:20.655894Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse",
                      updatedAt: "2020-11-19T20:02:29.112355Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LendView",
                      updatedAt: "2020-12-09T00:14:44.634529Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LTO Product",
                      updatedAt: "2021-05-10T08:37:27.459893Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "LTO Product / Land Titles Online (LTO) Backend",
                      updatedAt: "2021-05-10T23:47:03.142183Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnLand   ",
                      updatedAt: "2020-06-19T16:56:53.025018Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap",
                      updatedAt: "2020-06-29T18:16:21.718167Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Enterprise Geospatial",
                      updatedAt: "2020-12-10T20:10:03.253415Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PDS Product",
                      updatedAt: "2020-12-16T19:27:22.690535Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Pin Monitoring",
                      updatedAt: "2020-11-19T20:45:07.636259Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II",
                      updatedAt: "2020-06-16T20:46:27.631257Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS",
                      updatedAt: "2020-06-19T16:44:22.464520Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PPR Product",
                      updatedAt: "2020-12-16T19:26:27.285867Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ProSuite",
                      updatedAt: "2020-11-19T20:45:46.023720Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview",
                      updatedAt: "2020-12-10T19:47:43.859262Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery",
                      updatedAt: "2021-01-20T19:11:52.638390Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RedX",
                      updatedAt: "2020-11-19T21:29:01.072519Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RedX / RedX Web",
                      updatedAt: "2020-11-19T21:29:48.071219Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD",
                      updatedAt: "2020-06-29T19:15:26.666392Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC",
                      updatedAt: "2020-06-29T19:15:24.207954Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY HOST",
                      updatedAt: "2020-06-29T19:15:24.565656Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Rosco",
                      updatedAt: "2020-06-06T14:22:01.375632Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TCHK Product",
                      updatedAt: "2020-12-16T19:26:45.345882Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TCOL",
                      updatedAt: "2020-11-19T21:30:56.701534Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Express  ",
                      updatedAt: "2020-06-08T20:26:51.143878Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange",
                      updatedAt: "2020-11-19T21:31:33.656803Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview",
                      updatedAt: "2021-06-23T15:26:16.182491Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TSOL",
                      updatedAt: "2020-11-19T21:36:24.566562Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Billing",
                      updatedAt: "2020-12-08T15:28:11.945336Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law",
                      updatedAt: "2020-11-19T21:36:04.810425Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "WillBuilder",
                      updatedAt: "2020-11-19T21:38:53.452986Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs",
                      updatedAt: "2020-06-07T15:05:47.528752Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Business Layer",
              factsheets: {
                totalCount: 10,
                edges: [
                  {
                    node: {
                      displayName: "Polaris II / POLARIS BT",
                      updatedAt: "2020-06-12T14:31:31.868345Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS Reports",
                      updatedAt: "2020-06-17T20:22:06.763202Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS / RTS BT",
                      updatedAt: "2020-06-17T20:21:44.604979Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / Purview Reporting",
                      updatedAt: "2020-11-19T20:47:24.120169Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / TeranetXchange OnDemand",
                      updatedAt: "2020-11-19T21:33:06.877106Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / TeranetXchange TCopy",
                      updatedAt: "2020-11-19T21:33:36.672149Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teradmin BT",
                      updatedAt: "2020-06-16T20:40:31.154396Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Web BT",
                      updatedAt: "2020-07-28T13:58:13.195237Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs BT",
                      updatedAt: "2020-06-06T17:25:34.686215Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs NGBT",
                      updatedAt: "2020-06-07T19:45:49.333628Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Data Analytics/BI",
              factsheets: {
                totalCount: 20,
                edges: [
                  {
                    node: {
                      displayName: "CDC Framework",
                      updatedAt: "2020-06-18T12:43:02.342585Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CMS Registries Operational Dashboard",
                      updatedAt: "2020-08-12T19:38:36.650022Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ESR - Mapping KPI",
                      updatedAt: "2020-05-14T03:44:14.211102Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ESR - Performance Reporting",
                      updatedAt: "2020-05-14T03:44:14.315322Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - Commercial Revenue Report",
                      updatedAt: "2020-05-14T03:44:14.421984Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - Financial Business Forecasting",
                      updatedAt: "2020-08-12T19:34:21.411259Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - MOR Dashboard",
                      updatedAt: "2020-12-09T00:15:07.403915Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - Registration & Search Model",
                      updatedAt: "2020-05-14T03:44:14.640815Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - Weekly Volume Report",
                      updatedAt: "2020-05-14T03:44:14.738284Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview Billing",
                      updatedAt: "2020-12-09T00:18:29.875489Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Cognos",
                      updatedAt: "2020-06-29T19:15:28.923651Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Crystal Reports",
                      updatedAt: "2020-06-29T19:15:29.439099Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SSRS Reports Server",
                      updatedAt: "2020-06-05T02:12:45.864106Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY Reports",
                      updatedAt: "2020-06-29T19:15:24.632380Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS  AVM 2",
                      updatedAt: "2020-05-14T03:44:14.928688Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS CHMC Fraud Detection",
                      updatedAt: "2020-05-14T03:44:15.054217Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS LendView",
                      updatedAt: "2020-08-12T19:43:53.804433Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS OMI",
                      updatedAt: "2020-05-14T03:44:15.144727Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS Teranet HPI",
                      updatedAt: "2020-08-12T19:45:35.373526Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS Teranet Insight Report",
                      updatedAt: "2020-08-12T19:44:42.336298Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Data Source",
              factsheets: {
                totalCount: 89,
                edges: [
                  {
                    node: {
                      displayName: "Abstract Books Database",
                      updatedAt: "2021-02-18T16:13:49.812371Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD API Database",
                      updatedAt: "2020-09-11T13:23:41.987173Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Batch Database",
                      updatedAt: "2020-09-11T13:23:53.733209Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE Database",
                      updatedAt: "2020-06-09T18:47:25.182207Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE_Archive Database",
                      updatedAt: "2020-06-09T18:50:27.766277Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE_Audit Database",
                      updatedAt: "2020-06-09T18:51:01.380138Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE_Docs Database",
                      updatedAt: "2020-06-09T18:51:39.134567Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE_Logs Database",
                      updatedAt: "2020-06-09T18:54:18.270681Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / PDF2Data Database",
                      updatedAt: "2020-11-10T16:52:35.126135Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CSP Framework   / CSP DB  ",
                      updatedAt: "2020-06-06T14:23:32.867699Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Data Lake",
                      updatedAt: "2020-05-14T03:44:14.113184Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / eMap DB",
                      updatedAt: "2020-06-01T17:40:55.779407Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "File Server (PDF/TIFF/EFT File Repository)",
                      updatedAt: "2021-02-04T17:57:15.210051Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse DB",
                      updatedAt: "2020-12-08T15:06:02.423050Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Internal SSL Certificate Authority",
                      updatedAt: "2021-02-04T17:57:17.962398Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Land Titles Database",
                      updatedAt: "2021-02-18T16:14:47.443811Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LDAP Database",
                      updatedAt: "2021-02-04T17:57:24.150712Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / AMMR DB",
                      updatedAt: "2020-05-29T19:05:01.746893Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / Enterprise Geospatial / Delivery SDO DB",
                      updatedAt: "2020-12-10T20:10:03.248757Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / Enterprise Geospatial / Delivery ST DB",
                      updatedAt: "2020-12-10T20:10:03.240896Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / MTMS DB",
                      updatedAt: "2020-05-29T19:39:23.587883Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / OnMap DB",
                      updatedAt: "2020-05-29T19:46:00.704315Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS DB",
                      updatedAt: "2020-06-12T14:39:44.924919Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS / RTS DB",
                      updatedAt: "2020-06-16T20:42:16.166815Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PPR Database",
                      updatedAt: "2021-02-04T17:57:44.381126Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / DB",
                      updatedAt: "2020-11-19T20:46:28.003658Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / Reavs / Reavs DB",
                      updatedAt: "2020-11-19T20:47:39.481504Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AspState Database",
                      updatedAt: "2021-04-29T03:10:59.975750Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetAdmin Database",
                      updatedAt: "2020-06-05T02:44:29.947891Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetAudit Database",
                      updatedAt: "2020-06-12T20:52:54.368235Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetFax Database",
                      updatedAt: "2020-06-12T20:53:22.046871Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetFiles Database",
                      updatedAt: "2020-06-12T20:53:47.219655Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetSecurity Database",
                      updatedAt: "2020-06-12T20:54:09.896942Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Bankruptcy Database",
                      updatedAt: "2020-06-09T20:12:34.528790Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BankruptcyAdmin Database",
                      updatedAt: "2020-06-05T02:44:50.343818Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BankruptcyAudit Database",
                      updatedAt: "2020-06-09T20:11:48.248623Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BountyHunter Database",
                      updatedAt: "2020-06-03T20:34:18.760802Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CH Database",
                      updatedAt: "2020-06-05T02:59:10.741975Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CHTTAdmin Database",
                      updatedAt: "2020-06-05T02:46:07.917895Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CIBC Database",
                      updatedAt: "2020-06-05T02:57:30.212166Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ClientUpdateManager Database",
                      updatedAt: "2020-06-12T20:54:45.066646Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CMS_DEV_COGNOS_102 Database",
                      updatedAt: "2020-06-12T20:55:07.521280Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DBA Database",
                      updatedAt: "2020-06-12T20:55:26.933694Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Equipment Database",
                      updatedAt: "2020-06-12T20:56:11.932709Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / GlobalDataStore Database",
                      updatedAt: "2020-06-12T20:56:32.426259Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Hangfire Database",
                      updatedAt: "2020-06-05T02:37:47.010091Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ICAPRSL Database",
                      updatedAt: "2020-06-12T20:56:52.400158Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / LH Database",
                      updatedAt: "2020-06-05T02:59:37.572127Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PPSA Database",
                      updatedAt: "2020-06-09T20:12:38.964202Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PPSAAdmin Database",
                      updatedAt: "2020-06-05T02:46:57.068045Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RepoDepo Database",
                      updatedAt: "2020-06-29T19:15:32.966137Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ReportServer Database",
                      updatedAt: "2020-06-29T19:15:33.943696Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMS Db",
                      updatedAt: "2020-06-29T19:15:34.450287Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMS2 Database",
                      updatedAt: "2020-06-29T19:15:34.965660Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMSNOTES Database",
                      updatedAt: "2020-06-29T19:15:35.462220Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMSWI Database",
                      updatedAt: "2020-06-29T19:15:35.944678Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipLocator Database",
                      updatedAt: "2020-06-29T19:15:36.461559Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipLocatorAdmin Database",
                      updatedAt: "2020-06-29T19:15:36.960154Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SRQ Database",
                      updatedAt: "2020-06-29T19:15:37.467271Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / TaskService Database",
                      updatedAt: "2020-06-29T19:15:37.993253Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RedX / RedX DB",
                      updatedAt: "2020-11-19T21:29:18.414797Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD DataLake",
                      updatedAt: "2020-06-29T19:15:23.953273Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC Database",
                      updatedAt: "2020-06-29T19:15:24.090495Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY Database",
                      updatedAt: "2020-06-29T19:15:24.387607Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Database",
                      updatedAt: "2020-06-29T19:15:25.302693Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Streamloan Database",
                      updatedAt: "2020-06-29T19:15:25.892323Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD LVS SPAES Database",
                      updatedAt: "2020-09-17T15:51:39.768845Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD QMS Database",
                      updatedAt: "2020-09-17T15:52:26.586832Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Remove LiveCycle Originals",
                      updatedAt: "2021-05-20T02:22:24.527548Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Reusable File Server DB",
                      updatedAt: "2020-06-06T19:34:52.262394Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RMS Database",
                      updatedAt: "2021-02-18T16:15:14.248594Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SAP BW",
                      updatedAt: "2020-06-10T17:46:00.752337Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books Database",
                      updatedAt: "2021-02-18T16:15:31.810153Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / Teranet Xchange DB",
                      updatedAt: "2020-11-19T21:32:05.044232Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Blob Repository",
                      updatedAt: "2020-11-10T22:20:37.337594Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / NGTV BRMS DB",
                      updatedAt: "2020-06-05T22:18:02.465253Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Pass DB",
                      updatedAt: "2020-12-11T18:45:58.213691Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Audit Archived DB",
                      updatedAt: "2020-12-11T18:46:00.599998Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Billing DB",
                      updatedAt: "2020-06-05T21:44:25.586306Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Central DB",
                      updatedAt: "2020-06-08T15:27:46.433996Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Ltts DB",
                      updatedAt: "2020-12-11T18:46:03.637012Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Report DB",
                      updatedAt: "2020-12-11T18:46:16.464448Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR AD",
                      updatedAt: "2021-02-04T17:58:37.814527Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Database",
                      updatedAt: "2021-02-18T16:16:01.225101Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Web SiteTax Office Database (TOD)",
                      updatedAt: "2021-02-04T17:58:53.166584Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity Law DB",
                      updatedAt: "2020-11-19T21:37:51.183418Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / New WEF DB",
                      updatedAt: "2020-06-06T19:39:15.644006Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / WDF DB",
                      updatedAt: "2020-06-06T19:33:17.090776Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs DB",
                      updatedAt: "2020-12-08T15:12:28.958234Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Other Components",
              factsheets: {
                totalCount: 45,
                edges: [
                  {
                    node: {
                      displayName: "BulkArchiveSmoosher",
                      updatedAt: "2021-05-20T02:22:24.640866Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Batch Console",
                      updatedAt: "2020-09-11T13:23:14.144541Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Data Migration Process",
                      updatedAt: "2020-09-11T13:24:47.594257Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD LVS Report",
                      updatedAt: "2020-09-11T13:24:57.045276Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Middleware Dashboard",
                      updatedAt: "2020-09-08T13:09:47.554878Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Daily Revenue Entry",
                      updatedAt: "2021-02-18T18:10:53.257995Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Do Process Wrapper",
                      updatedAt: "2020-06-29T15:58:04.463805Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "E-ComUnity",
                      updatedAt: "2020-06-29T15:59:14.766188Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "EFT File Creation",
                      updatedAt: "2021-02-18T18:10:55.792448Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "EFT File Creation Service",
                      updatedAt: "2021-02-18T18:10:59.731584Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Log Monitor",
                      updatedAt: "2020-06-05T19:13:35.125841Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse Reporting",
                      updatedAt: "2020-11-19T20:08:43.148940Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Map Tile Service",
                      updatedAt: "2020-11-19T20:14:13.090856Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Geowarehouse / Map Tile Service / Geomedia WebMap",
                      updatedAt: "2020-11-19T20:18:06.166327Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Google Analytics",
                      updatedAt: "2020-11-19T20:42:12.243645Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Knowledge Base",
                      updatedAt: "2021-02-18T18:10:35.408792Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PPR Product / Personal Property Registry (PPR) UI / ReDistribition",
                      updatedAt: "2021-02-18T17:13:50.079304Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Print Manager  ",
                      updatedAt: "2020-06-06T14:04:10.742765Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RBC FTP Maintenance Task",
                      updatedAt: "2021-05-20T02:22:24.617981Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RDPRM",
                      updatedAt: "2020-10-19T20:03:03.981065Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Report Viewer",
                      updatedAt: "2020-06-29T19:15:33.475604Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD BBY CSRS RPA",
                      updatedAt: "2020-06-29T19:15:23.918709Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD KYC / CD KYC Automation GUI",
                      updatedAt: "2020-06-29T19:15:24.023781Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD KYC / CD KYC Service Scheduler",
                      updatedAt: "2020-06-29T19:15:24.164193Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY DOSPPSA",
                      updatedAt: "2020-06-29T19:15:24.440747Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy BBY File Dispatcher",
                      updatedAt: "2020-06-29T19:15:24.501759Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Automation GUI",
                      updatedAt: "2020-06-29T19:15:25.022509Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Batch Console",
                      updatedAt: "2020-06-29T19:15:25.108930Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy MDA",
                      updatedAt: "2020-06-29T19:15:25.680337Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy RDPRM",
                      updatedAt: "2020-06-29T19:15:25.787786Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Streamloan Document Generator",
                      updatedAt: "2020-06-29T19:15:25.997060Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD LVS SPAES Web Service",
                      updatedAt: "2020-09-17T15:52:10.088631Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Scheduled FTP Service",
                      updatedAt: "2021-02-18T18:11:44.973051Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Scheduled Print Service",
                      updatedAt: "2021-02-18T18:12:18.147558Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Scheduled Report Service",
                      updatedAt: "2021-02-18T18:13:43.400013Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Exports",
                      updatedAt: "2021-02-18T18:13:20.649116Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR PDF Splitter service",
                      updatedAt: "2021-02-18T18:13:23.785570Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Reports/TPR Exports/Update XLSX",
                      updatedAt: "2021-02-18T18:13:27.007555Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Scan and Trigger service",
                      updatedAt: "2021-02-18T18:13:29.656243Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Workflow Document service",
                      updatedAt: "2021-02-18T18:13:51.169768Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Transaction Batch Acceptance Rejection (TBAR)",
                      updatedAt: "2021-02-18T18:14:30.090633Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity PI",
                      updatedAt: "2020-11-19T21:38:21.219318Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Workflow Initialization Service",
                      updatedAt: "2021-05-20T02:22:24.583679Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Workflow Registration Service",
                      updatedAt: "2021-05-20T02:22:24.558769Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / ReportGen",
                      updatedAt: "2020-06-06T19:28:33.226166Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "DevOps/SDLC Tool",
              factsheets: {
                totalCount: 31,
                edges: [
                  {
                    node: {
                      displayName: "Aha! Cloud",
                      updatedAt: "2020-07-13T15:09:07.387905Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ALMQC",
                      updatedAt: "2020-06-17T13:32:40.780457Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Artifactory",
                      updatedAt: "2020-07-13T18:16:48.479737Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Atlassian Bitbucket",
                      updatedAt: "2020-12-11T19:20:33.540143Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Atlassian Confluence",
                      updatedAt: "2020-12-11T19:12:43.871277Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Atlassian JIRA",
                      updatedAt: "2020-12-11T19:24:39.630880Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Beyond Compare",
                      updatedAt: "2020-12-11T20:39:42.076515Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Blueprint Storyteller",
                      updatedAt: "2020-06-17T13:42:25.589832Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ConnectALL Integration Platform",
                      updatedAt: "2020-06-17T13:34:30.986197Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Dependency Checker",
                      updatedAt: "2020-05-14T04:15:27.678167Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Entity Framework",
                      updatedAt: "2020-12-11T22:13:59.795310Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Entity Framework Core",
                      updatedAt: "2020-12-11T22:13:27.408290Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Gerrit",
                      updatedAt: "2020-06-17T13:12:50.500673Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Git",
                      updatedAt: "2020-06-17T13:20:23.570021Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "JAWS",
                      updatedAt: "2020-05-14T04:15:28.236943Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Jenkins CI",
                      updatedAt: "2020-07-13T18:22:21.067959Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Micro Focus Fortify",
                      updatedAt: "2020-07-20T17:59:48.710559Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Azure DevOps Tools Cloud",
                      updatedAt: "2020-12-11T19:05:41.680751Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Azure DevOps Tools onprem 2019",
                      updatedAt: "2020-12-11T19:32:53.640004Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Azure DevOps Tools onprem 2020",
                      updatedAt: "2020-12-11T18:52:53.053426Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Visual Studio",
                      updatedAt: "2021-01-19T12:41:23.676189Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Visual Studio Code",
                      updatedAt: "2020-12-11T21:01:03.962883Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OWASP ZAP",
                      updatedAt: "2020-05-14T04:15:28.827986Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Resharper Ultimate",
                      updatedAt: "2020-12-11T20:53:16.253974Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Selenium Framework",
                      updatedAt: "2021-02-18T18:23:52.239288Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SmartBear ReadyAPI!",
                      updatedAt: "2020-05-14T04:15:29.415685Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SmartGit",
                      updatedAt: "2020-06-17T13:25:34.847906Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Sparx Enterprise Architect",
                      updatedAt: "2020-05-14T04:15:30.007601Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Tesim.io Cloud",
                      updatedAt: "2020-07-17T19:14:52.622442Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "The IDE (Eclipse IDE)",
                      updatedAt: "2020-12-11T19:58:09.403651Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "XMLSpy",
                      updatedAt: "2020-05-14T04:15:30.296079Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Presentation Layer - Web App",
              factsheets: {
                totalCount: 48,
                edges: [
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Admin Dashboard Web",
                      updatedAt: "2020-09-11T13:22:51.707341Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / Collateral Guard Enterprise (CGE) Web",
                      updatedAt: "2021-02-23T11:43:59.236421Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eWorkflow",
                      updatedAt: "2021-02-04T17:57:13.848891Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / HeatMaps",
                      updatedAt: "2020-11-19T20:17:38.728683Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "LTO Product / Land Titles Online (LTO) Backend",
                      updatedAt: "2021-05-10T23:47:03.142183Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnLand    / Onland CMS  ",
                      updatedAt: "2020-06-05T19:52:54.751704Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Control Panel",
                      updatedAt: "2020-06-05T19:27:43.139259Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / MTMS - Mapping Task Management System",
                      updatedAt: "2020-05-29T19:37:52.406614Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Paytickets",
                      updatedAt: "2020-06-29T16:10:12.664249Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS PT",
                      updatedAt: "2020-06-12T14:24:59.106229Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS / RTS PT",
                      updatedAt: "2020-06-16T20:20:50.276302Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / Purview Application",
                      updatedAt: "2020-11-19T20:46:45.088114Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / Reavs",
                      updatedAt: "2020-11-19T21:26:51.157534Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset Admin",
                      updatedAt: "2021-04-29T03:10:59.981675Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Bankruptcy Highway",
                      updatedAt: "2020-06-29T19:15:21.091552Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Collection Highway",
                      updatedAt: "2020-06-03T22:41:55.166205Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Document Highway",
                      updatedAt: "2020-06-03T22:40:54.463956Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Litigation Highway",
                      updatedAt: "2020-06-03T23:12:12.793377Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Optimus Portal",
                      updatedAt: "2020-06-29T19:15:30.980865Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Payment Highway",
                      updatedAt: "2020-06-04T00:06:12.763073Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Property Highway",
                      updatedAt: "2020-06-29T19:15:32.450113Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Registries Online",
                      updatedAt: "2020-06-04T23:16:38.748105Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Repo Highway",
                      updatedAt: "2020-06-04T23:21:53.093075Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Skip Locator",
                      updatedAt: "2020-06-04T23:59:03.139542Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SRQ",
                      updatedAt: "2020-06-05T02:11:41.454289Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD BAS",
                      updatedAt: "2020-06-29T19:15:23.891303Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC Admin Web",
                      updatedAt: "2020-06-29T19:15:23.994290Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC Client Web",
                      updatedAt: "2020-06-29T19:15:24.057575Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy BBY CSRS Interanet Web (JSP)",
                      updatedAt: "2020-06-29T19:15:24.275624Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy BBY CSRS Web (ASPX)",
                      updatedAt: "2020-06-29T19:15:24.329960Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Admin Web",
                      updatedAt: "2020-06-29T19:15:24.941257Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Client Web",
                      updatedAt: "2020-06-29T19:15:25.198578Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy CWI TDAF Web",
                      updatedAt: "2020-06-29T19:15:25.483827Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy CWI TDFS Web",
                      updatedAt: "2020-06-29T19:15:25.590912Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy Streamloan Web",
                      updatedAt: "2020-06-29T19:15:26.102078Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD QMS Regsitry Web",
                      updatedAt: "2020-09-17T15:52:36.796445Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD QMS Universal Web",
                      updatedAt: "2020-09-17T15:52:50.041062Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Rosco / Rosco PT  ",
                      updatedAt: "2020-06-19T16:52:57.424556Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Smart Forms Transfer (T), Mortgage (M), Caveat (C), Discharge (D)",
                      updatedAt: "2021-02-04T17:58:14.199935Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Express   / Teranet Express PT  ",
                      updatedAt: "2020-06-06T15:03:53.202819Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / Teranet Xchange Online",
                      updatedAt: "2020-11-19T21:32:22.327608Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / Teranet Xchange Web",
                      updatedAt: "2020-11-19T21:32:55.850371Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / CSC Teradmin PT",
                      updatedAt: "2020-09-30T14:08:54.451703Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Management Report",
                      updatedAt: "2020-06-05T20:59:41.236378Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / PortasAdmin",
                      updatedAt: "2020-06-16T20:41:55.104565Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Web eReg PT",
                      updatedAt: "2020-06-19T15:34:02.412305Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Web LRO PT",
                      updatedAt: "2020-06-06T01:20:56.992075Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "tprmb.ca Web Site",
                      updatedAt: "2021-02-04T17:58:57.509874Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Presentation Layer - Desktop App",
              factsheets: {
                totalCount: 12,
                edges: [
                  {
                    node: {
                      displayName: "OnMap / Desktop & Addins",
                      updatedAt: "2020-06-11T13:12:57.313680Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Get Image",
                      updatedAt: "2020-06-11T13:03:37.102539Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS / RTS Desktop",
                      updatedAt: "2020-06-16T20:41:17.330922Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset Registry",
                      updatedAt: "2020-06-09T20:09:57.835012Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset RMS",
                      updatedAt: "2020-06-05T02:59:50.617308Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Bankruptcy EXE",
                      updatedAt: "2020-06-29T19:15:20.648271Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RedX / RedX Desktop",
                      updatedAt: "2020-11-19T21:29:33.183518Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY WinBill",
                      updatedAt: "2020-06-29T19:15:24.702517Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY WINPPSA",
                      updatedAt: "2020-06-29T19:15:24.786102Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB)",
                      updatedAt: "2021-02-18T18:14:26.594504Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Tax Office Database (TOD)",
                      updatedAt: "2021-02-08T15:03:59.088439Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client",
                      updatedAt: "2021-02-18T18:12:54.382362Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "API Service Implementation",
              factsheets: {
                totalCount: 68,
                edges: [
                  {
                    node: {
                      displayName: "AEM Document Data Interface Service",
                      updatedAt: "2020-12-17T15:54:22.391275Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "AEM Notification Service",
                      updatedAt: "2020-12-17T15:54:35.126258Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "AEM Workflow Service",
                      updatedAt: "2020-12-17T15:54:11.240549Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CDS",
                      updatedAt: "2020-06-19T16:44:22.452465Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ClamAV Service",
                      updatedAt: "2021-02-04T17:57:04.184432Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD REST API",
                      updatedAt: "2020-12-11T21:28:16.802936Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD SOAP API",
                      updatedAt: "2020-09-11T13:25:14.578354Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE REST API (Internal)",
                      updatedAt: "2021-06-16T01:02:53.505488Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE Web Services",
                      updatedAt: "2021-02-23T11:43:13.156390Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / Pdf2DataAPI",
                      updatedAt: "2020-11-10T16:52:53.649650Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CPS",
                      updatedAt: "2020-06-18T00:33:41.309795Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CRS",
                      updatedAt: "2020-11-27T21:41:51.030682Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Document Data Interface(DDI)",
                      updatedAt: "2021-02-04T17:57:07.353725Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Elastic Search Service",
                      updatedAt: "2020-06-19T11:53:18.178671Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Server & Customizations",
                      updatedAt: "2020-06-15T14:13:36.229052Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FileStream",
                      updatedAt: "2021-02-18T18:09:38.218830Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse Api",
                      updatedAt: "2020-11-19T20:08:11.863271Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "IOService",
                      updatedAt: "2021-02-18T18:11:24.380533Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Mail Service",
                      updatedAt: "2021-02-18T18:10:38.217458Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "New TSB",
                      updatedAt: "2021-07-07T14:57:37.038242Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "New TSB Cloud",
                      updatedAt: "2021-07-07T14:58:00.382289Z",
                      qualitySeal: "APPROVED",
                    },
                  },
                  {
                    node: {
                      displayName: "OnLand    / Onland API  ",
                      updatedAt: "2020-06-05T19:47:01.175420Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Legal Text Parser",
                      updatedAt: "2020-05-29T19:35:45.739589Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Task Manager",
                      updatedAt: "2020-06-11T13:05:28.661515Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Order Service",
                      updatedAt: "2021-02-18T18:10:44.320078Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PDFFlatteningService",
                      updatedAt: "2021-02-18T18:10:48.667614Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PDS Product / Plan Deposit Submission (PDS)  Backend",
                      updatedAt: "2021-02-18T18:17:12.485171Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PINService",
                      updatedAt: "2020-06-19T12:00:53.035933Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PPR Batch",
                      updatedAt: "2021-02-04T17:57:42.716330Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Print Queues",
                      updatedAt: "2021-02-04T17:57:45.634065Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Printed Output",
                      updatedAt: "2021-02-18T18:11:26.935809Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PromoteReports",
                      updatedAt: "2021-05-20T02:22:24.604570Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Account Maintenance Service (Recovery Services)",
                      updatedAt: "2021-04-29T03:11:00.001342Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset Reporting",
                      updatedAt: "2020-06-29T19:15:13.299118Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset RMS Web Services",
                      updatedAt: "2020-06-29T19:15:13.700445Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Bankrupt Account Setup Validation Service (Recovery Services)",
                      updatedAt: "2020-06-29T19:15:20.199379Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Collection Services Api Service (Recovery Services)",
                      updatedAt: "2020-06-04T02:34:58.054597Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Credit Beaureu Service (Recovery External Services)",
                      updatedAt: "2020-06-04T23:21:41.178238Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH BlackBook Service (Recovery External Services)",
                      updatedAt: "2020-06-03T22:11:33.908556Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Direct Payment Post File Service (Recovery Services)",
                      updatedAt: "2020-06-29T19:15:29.940834Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / File Transfer Service",
                      updatedAt: "2020-06-05T02:54:50.039901Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Letter Service (Recovery Services)",
                      updatedAt: "2020-06-03T23:04:10.189979Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Litigation Services Api Service (Recovery Services)",
                      updatedAt: "2020-06-03T23:14:14.023114Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Optimus Web Services",
                      updatedAt: "2020-06-05T02:18:39.066866Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / ReportLog Service (Asset Audit International Services)",
                      updatedAt: "2020-06-04T23:25:03.784076Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / ReportLog Service (Asset Audit Services)",
                      updatedAt: "2020-06-04T23:58:08.447074Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / RH Audit Log Service (Asset Audit International Services)",
                      updatedAt: "2020-06-04T23:31:11.805305Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / RH Audit Log Service (Asset Audit Services)",
                      updatedAt: "2020-06-04T23:33:33.709541Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Statement Generation Service",
                      updatedAt: "2020-06-05T02:16:18.039067Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / UserActivity Service (Asset Audit International Services)",
                      updatedAt: "2020-06-05T02:23:09.944918Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / UserActivity Service (Asset Audit Services)",
                      updatedAt: "2020-06-05T02:25:17.360926Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registration Support service",
                      updatedAt: "2021-02-18T18:11:36.111324Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC REST API",
                      updatedAt: "2020-06-29T19:15:24.126378Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC Web Service",
                      updatedAt: "2020-06-29T19:15:24.206016Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Web Service",
                      updatedAt: "2020-06-29T19:15:25.392503Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Relay Email/Fax Server",
                      updatedAt: "2021-02-04T17:57:57.674133Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Reusable File Server",
                      updatedAt: "2020-06-07T15:00:21.901265Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SQL Reporting System (SSRS)",
                      updatedAt: "2021-02-04T17:58:15.947069Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SurveyBooks Documents Service",
                      updatedAt: "2021-02-18T18:12:51.214470Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TCHK Product / Title Check (TCHK) Backend",
                      updatedAt: "2021-02-18T18:18:59.408516Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / CAS",
                      updatedAt: "2020-12-16T15:16:15.898250Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / DAWS",
                      updatedAt: "2021-06-23T15:26:20.682065Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / NGTV BRMS",
                      updatedAt: "2020-06-05T22:20:15.347897Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Service",
                      updatedAt: "2021-02-18T18:13:33.593856Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TSB",
                      updatedAt: "2021-07-07T14:57:36.911852Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity Law API",
                      updatedAt: "2020-12-10T14:20:28.789897Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "WorkflowWrapperService",
                      updatedAt: "2021-02-18T18:14:40.518414Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Remote Access Service (RAAC)",
                      updatedAt: "2020-06-07T15:02:39.770553Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Background Job/Daemon",
              factsheets: {
                totalCount: 151,
                edges: [
                  {
                    node: {
                      displayName: "ArchiveBackendScans",
                      updatedAt: "2021-05-20T02:22:24.645716Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ArchiveOldReports",
                      updatedAt: "2021-05-20T02:22:24.631272Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Assessment File System",
                      updatedAt: "2021-02-18T18:08:17.454927Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "AutoReconciliation Service",
                      updatedAt: "2021-02-18T18:08:31.523971Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE Hangfire",
                      updatedAt: "2021-02-23T11:41:35.909623Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Daily Update",
                      updatedAt: "2020-06-15T14:42:51.778014Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Health Check",
                      updatedAt: "2020-06-01T17:32:27.833694Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Log Analyzer",
                      updatedAt: "2020-06-11T13:06:32.327506Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Log Archiving",
                      updatedAt: "2020-06-01T17:33:40.751794Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / PIN Notification",
                      updatedAt: "2020-06-11T13:07:05.074061Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Notification Service",
                      updatedAt: "2021-05-20T02:22:24.664088Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / AMMR - Automated Map Maintenance Request",
                      updatedAt: "2020-06-11T13:01:04.908183Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Enterprise Geospatial / Dengine",
                      updatedAt: "2020-12-10T20:10:03.256500Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / Enterprise Geospatial / Tile Generation",
                      updatedAt: "2020-12-10T20:10:03.244942Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / JPS - Job Processing Service",
                      updatedAt: "2020-06-11T13:04:08.274063Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Maintenance",
                      updatedAt: "2020-11-24T14:13:14.624467Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Mapping Title Update",
                      updatedAt: "2020-06-11T13:04:57.319581Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Monitoring",
                      updatedAt: "2020-06-05T18:18:42.323459Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / PAT - Parcel Automation Tool",
                      updatedAt: "2020-06-05T18:18:07.754Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Title Data Warehouse DB",
                      updatedAt: "2020-05-29T19:53:37.573184Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS Title Update",
                      updatedAt: "2020-06-17T14:59:54.208816Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Active_Download_vs2008",
                      updatedAt: "2021-04-29T03:10:59.995666Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.BankruptcyDailyWorklist",
                      updatedAt: "2021-04-21T01:43:37.877777Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.BNSInvalidPhoneLoad",
                      updatedAt: "2021-04-21T01:43:37.895464Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.BNSScorecard",
                      updatedAt: "2021-04-21T01:43:37.884677Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.CACSCloseout",
                      updatedAt: "2020-11-03T13:15:51.877070Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.DailyUserPerformances",
                      updatedAt: "2021-04-21T01:43:37.910436Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.FifthCycleBillingAutoTrigger",
                      updatedAt: "2020-06-29T19:15:16.174229Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.File.Upload.Validation",
                      updatedAt: "2020-06-29T19:15:16.636957Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.MOCBatchQ",
                      updatedAt: "2020-11-03T13:12:47.519130Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.RBCBOCLoad",
                      updatedAt: "2020-11-03T13:14:28.319954Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.RBCPreAuthorizedPayment",
                      updatedAt: "2021-04-21T01:43:37.900538Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.ShawResponseTrigger",
                      updatedAt: "2020-06-29T19:15:18.334507Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.TrendMonthly",
                      updatedAt: "2020-06-29T19:15:18.774672Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ASSETWorkTrace",
                      updatedAt: "2021-04-29T03:11:00.014648Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AutoQNationalSales",
                      updatedAt: "2020-06-29T19:15:19.773126Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BH_PostBatch",
                      updatedAt: "2020-06-29T19:15:21.544505Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BHLoanNumberFix",
                      updatedAt: "2020-06-29T19:15:21.980441Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BHUserSynchronize",
                      updatedAt: "2020-06-29T19:15:22.428564Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Billing",
                      updatedAt: "2020-06-29T19:15:22.909262Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BKNoTaskScan",
                      updatedAt: "2020-06-29T19:15:23.393452Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BlackBookUpload",
                      updatedAt: "2020-06-03T22:40:17.235011Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BMO_Export_MC_File",
                      updatedAt: "2020-06-03T20:11:23.306832Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BmoDraft5FileExport",
                      updatedAt: "2020-06-03T20:12:46.710551Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BmoFileExport",
                      updatedAt: "2020-06-05T03:12:26.409994Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BmoFileImport",
                      updatedAt: "2020-06-03T20:15:46.595294Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BNS_Contingency",
                      updatedAt: "2020-06-04T01:35:22.741598Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BNSDebtManager2007App",
                      updatedAt: "2020-06-29T19:15:23.867586Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BNSPaymentApp",
                      updatedAt: "2020-06-05T03:12:43.461295Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BNSRedistributionBatch",
                      updatedAt: "2020-06-03T22:39:56.907234Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CCSBatch_GL",
                      updatedAt: "2020-06-03T20:40:34.736581Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CCSBatch_Tasks",
                      updatedAt: "2020-06-03T22:40:35.824422Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CCSBOCOverpaymentComplete",
                      updatedAt: "2020-06-04T01:35:55.623014Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CH_AccruedInterest",
                      updatedAt: "2020-06-04T01:37:34.815390Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CH_JudgmentAcrIntrst",
                      updatedAt: "2020-06-03T20:58:06.069527Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / ChAgencyAssignmentPostChargeOffCalculation",
                      updatedAt: "2020-06-29T19:15:27.487856Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CHAgencyUpload",
                      updatedAt: "2020-06-29T19:15:27.959265Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CHAGenericFileImport",
                      updatedAt: "2020-06-03T22:42:48.363324Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CIBC.Link.Security",
                      updatedAt: "2020-06-03T22:42:31.075835Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CIBC_Batch",
                      updatedAt: "2020-06-03T21:13:56.932610Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CleanListBatchApp",
                      updatedAt: "2020-06-03T22:42:13.624515Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ClientPrimeIntRate",
                      updatedAt: "2020-06-03T21:30:09.224820Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ClientReconcilationReport",
                      updatedAt: "2020-06-29T19:15:28.447007Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CopyFileUP",
                      updatedAt: "2020-06-04T01:38:13.639222Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CreditBureauReportFileExport",
                      updatedAt: "2020-06-03T22:41:31.906119Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Critical.Batch",
                      updatedAt: "2020-06-03T22:41:13.237884Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DepositBatch",
                      updatedAt: "2020-06-03T22:10:52.528421Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.AssignmentScoreFileImportAndExport",
                      updatedAt: "2021-04-22T02:50:45.776674Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.BNSDebtManagerFileImport",
                      updatedAt: "2021-04-22T02:50:45.767464Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.BNSOutputFiles",
                      updatedAt: "2021-04-22T02:50:45.748918Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DH.RecoveryServices.CACSImport",
                      updatedAt: "2021-04-22T02:50:45.787996Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.CentralizedFileImporter",
                      updatedAt: "2021-04-22T02:50:45.782449Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.DataConversionReports",
                      updatedAt: "2021-04-22T02:50:45.758683Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.DirectPaymentAutoRemit_DH",
                      updatedAt: "2021-01-21T11:48:18.449201Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.DirectPaymentPosting",
                      updatedAt: "2021-04-22T02:50:45.792287Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.Galaxy.AgencyCodeAssignment",
                      updatedAt: "2020-06-04T02:20:02.941338Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.GSLACDebtmanager",
                      updatedAt: "2020-06-04T02:21:48.002459Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.HondaFileImport",
                      updatedAt: "2020-06-04T02:25:38.179459Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.HSBCFileImport",
                      updatedAt: "2020-06-04T02:28:43.434539Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.HSBCNotesAndDataBackup",
                      updatedAt: "2020-06-04T02:31:13.151478Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RBCAutoClosure",
                      updatedAt: "2020-06-04T02:33:03.811037Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RBCDirectPaymentPosting",
                      updatedAt: "2021-04-21T01:43:37.892220Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RBCPapPosting",
                      updatedAt: "2021-04-21T01:43:37.890216Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RBCResolveFileImport",
                      updatedAt: "2021-04-21T01:43:37.908726Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.Redistribution",
                      updatedAt: "2020-06-04T02:42:05.890745Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RepoDepoInventoryExport",
                      updatedAt: "2020-06-04T02:49:55.077964Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.TDAFAssignmentFileToMDB",
                      updatedAt: "2020-06-04T02:51:54.331279Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.TDAFFileImport",
                      updatedAt: "2020-06-04T02:54:06.181912Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.TDAFLegacyAssignmentFiletoMDB",
                      updatedAt: "2020-06-04T02:55:24.341778Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DH.RecoveryServices.TS2Import",
                      updatedAt: "2020-06-04T02:57:10.689506Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DirectPaymentAutoProcessBatch",
                      updatedAt: "2020-06-03T22:18:23.040427Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DirectPaymentAutoRemit",
                      updatedAt: "2020-06-03T22:19:53.156983Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ExtinguishedDebt",
                      updatedAt: "2020-06-03T22:31:43.460957Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / GLPost",
                      updatedAt: "2020-06-03T22:47:15.140679Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / HSBC_Monthly_DataFeed",
                      updatedAt: "2020-06-03T22:53:26.242313Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / LetterBatch",
                      updatedAt: "2020-06-03T23:02:18.731128Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / LogInterestRate",
                      updatedAt: "2020-06-05T03:13:00.064149Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / New_Download_vs2008",
                      updatedAt: "2020-06-03T23:41:44.553113Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / NightlyBatch.AutoXBF",
                      updatedAt: "2020-06-29T19:15:30.401776Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / NightlyBatch.SalesTaskMaintenance",
                      updatedAt: "2020-06-03T23:44:24.376368Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / NightlyBatchThunder",
                      updatedAt: "2020-06-03T23:49:39.320534Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Optimus Windows Service",
                      updatedAt: "2020-06-03T23:56:27.771212Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / OutstandingAsgn",
                      updatedAt: "2020-06-04T00:02:19.192599Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PH_SLInvoiceMiluGenerator",
                      updatedAt: "2020-06-04T00:08:15.767058Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PrimeRateChangeLowestInterest",
                      updatedAt: "2020-06-29T19:15:31.961675Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RBC_Intmast",
                      updatedAt: "2020-06-04T01:07:23.702470Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RBC_Redistribution",
                      updatedAt: "2020-06-04T01:08:32.423767Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RbcFileExport",
                      updatedAt: "2020-06-04T01:09:50.740401Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RBCGLBatch_Process",
                      updatedAt: "2020-06-04T01:11:02.991924Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RBCIThirdPartyLoad",
                      updatedAt: "2020-06-04T01:12:56.776549Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Rdstrbtn_CleanUP",
                      updatedAt: "2020-06-04T01:14:10.531483Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RecalcColInvPerDiem",
                      updatedAt: "2020-06-04T01:15:25.895306Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RecalcPerDiemJudgmnt",
                      updatedAt: "2020-06-04T01:17:45.112202Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Recovery Hangfire",
                      updatedAt: "2020-06-09T19:40:26.363956Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Redistribution",
                      updatedAt: "2020-06-04T17:09:58.240152Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RevokeStatus",
                      updatedAt: "2020-06-04T23:29:45.707352Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RHCostRecoveryGLInvoice",
                      updatedAt: "2020-06-04T23:43:29.869460Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMSABDemandRelocateScan",
                      updatedAt: "2020-06-04T23:50:43.036105Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMSTaskMissing",
                      updatedAt: "2020-06-04T23:52:23.321898Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SDALegacyLoad",
                      updatedAt: "2020-06-04T23:54:09.043504Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SetlementStatus",
                      updatedAt: "2020-06-04T23:56:01.040901Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Skip_NightBatch",
                      updatedAt: "2020-06-05T00:02:47.654150Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipBankReview",
                      updatedAt: "2020-06-05T02:04:00.202104Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipLocatorDTS",
                      updatedAt: "2020-06-05T02:05:23.301845Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipNewAssignment",
                      updatedAt: "2020-06-05T02:09:32.833949Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / StatementBatch",
                      updatedAt: "2020-06-05T02:17:29.101588Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / TravelersAssignments",
                      updatedAt: "2020-06-05T02:18:47.408227Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / TravelersResponse",
                      updatedAt: "2020-06-05T02:19:42.582077Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / TyMetrixBatch",
                      updatedAt: "2020-06-05T02:20:36.879760Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / WorkflowAutomation",
                      updatedAt: "2020-06-05T02:29:34.650753Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books Update Document Service",
                      updatedAt: "2021-02-18T18:12:47.901397Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / Teranet Xchange Batch",
                      updatedAt: "2020-11-19T21:31:47.335035Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Blob Sync",
                      updatedAt: "2020-06-16T20:33:44.258098Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Bookloading",
                      updatedAt: "2020-06-16T20:35:03.340746Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Bulk Reporting",
                      updatedAt: "2020-06-16T20:35:28.828872Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Database AppBatch Jobs",
                      updatedAt: "2020-06-05T19:51:11.136904Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / EFT",
                      updatedAt: "2020-06-16T20:37:29.849691Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / GL",
                      updatedAt: "2020-06-16T20:37:09.658339Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Image Upload",
                      updatedAt: "2020-07-28T13:59:30.402858Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / jBlobsvr",
                      updatedAt: "2020-07-14T14:54:08.198957Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / jCert",
                      updatedAt: "2020-06-16T20:36:18.403674Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Teraview / Long Term Signature Verification",
                      updatedAt: "2020-06-16T20:43:19.184349Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Lsuc/AOLS Scripts",
                      updatedAt: "2020-06-05T22:03:49.471049Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / LTTSXML",
                      updatedAt: "2020-06-19T15:35:43.317032Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Password Change Scripts",
                      updatedAt: "2020-06-05T21:19:34.606928Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Teraview / Recovery - Pending Registrations",
                      updatedAt: "2020-06-05T21:25:25.936951Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Request Processor",
                      updatedAt: "2020-12-04T16:58:58.686187Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Server Monitor",
                      updatedAt: "2020-06-16T20:41:12.112878Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / TD Diamond",
                      updatedAt: "2020-06-16T20:40:54.865622Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Docgen",
                      updatedAt: "2020-11-19T21:37:27.440947Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / WDF",
                      updatedAt: "2020-06-06T19:27:59.321582Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Important",
              factsheets: {
                totalCount: 1,
                edges: [
                  {
                    node: {
                      displayName: "Finance Service 10.5",
                      updatedAt: "2021-06-25T01:58:38.017174Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "TMB",
              factsheets: {
                totalCount: 98,
                edges: [
                  {
                    node: {
                      displayName: "Abstract Books Database",
                      updatedAt: "2021-02-18T16:13:49.812371Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "AEM Document Data Interface Service",
                      updatedAt: "2020-12-17T15:54:22.391275Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "AEM Notification Service",
                      updatedAt: "2020-12-17T15:54:35.126258Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "AEM Workflow Service",
                      updatedAt: "2020-12-17T15:54:11.240549Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ArchiveBackendScans",
                      updatedAt: "2021-05-20T02:22:24.645716Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ArchiveOldReports",
                      updatedAt: "2021-05-20T02:22:24.631272Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Assessment File System",
                      updatedAt: "2021-02-18T18:08:17.454927Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "AUTH Product",
                      updatedAt: "2020-12-16T19:28:36.126030Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "AUTH Product / Self Service Password Reset (SSPR) / AUTH Site Backend",
                      updatedAt: "2021-02-18T18:16:49.545109Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "AUTH Product / Self Service Password Reset (SSPR) / AUTH Site UI",
                      updatedAt: "2020-12-16T14:55:04.613273Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "AutoReconciliation Service",
                      updatedAt: "2021-02-18T18:08:31.523971Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "BulkArchiveSmoosher",
                      updatedAt: "2021-05-20T02:22:24.640866Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ClamAV Service",
                      updatedAt: "2021-02-04T17:57:04.184432Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Daily Revenue Entry",
                      updatedAt: "2021-02-18T18:10:53.257995Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Document Data Interface(DDI)",
                      updatedAt: "2021-02-04T17:57:07.353725Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "EFT File Creation",
                      updatedAt: "2021-02-18T18:10:55.792448Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "EFT File Creation Service",
                      updatedAt: "2021-02-18T18:10:59.731584Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "EREG Product",
                      updatedAt: "2020-12-16T19:27:41.878640Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "EREG Product / eRegistration (eReg) Backend",
                      updatedAt: "2021-02-18T18:17:35.951106Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "EREG Product / eRegistration (eReg) UI",
                      updatedAt: "2020-12-16T14:53:29.454232Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eWorkflow",
                      updatedAt: "2021-02-04T17:57:13.848891Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "File Server (PDF/TIFF/EFT File Repository)",
                      updatedAt: "2021-02-04T17:57:15.210051Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FileStream",
                      updatedAt: "2021-02-18T18:09:38.218830Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Internal SSL Certificate Authority",
                      updatedAt: "2021-02-04T17:57:17.962398Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "IOService",
                      updatedAt: "2021-02-18T18:11:24.380533Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Knowledge Base",
                      updatedAt: "2021-02-18T18:10:35.408792Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Land Titles Database",
                      updatedAt: "2021-02-18T16:14:47.443811Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LDAP Database",
                      updatedAt: "2021-02-04T17:57:24.150712Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LTO Product",
                      updatedAt: "2021-05-10T08:37:27.459893Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "LTO Product / Land Titles Online (LTO) Backend",
                      updatedAt: "2021-05-10T23:47:03.142183Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LTO Product / Land Titles Online (LTO) UI",
                      updatedAt: "2020-12-16T14:49:25.746915Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Mail Service",
                      updatedAt: "2021-02-18T18:10:38.217458Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "MLTS",
                      updatedAt: "2021-02-04T17:57:34.436190Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Notification Service",
                      updatedAt: "2021-05-20T02:22:24.664088Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Order Service",
                      updatedAt: "2021-02-18T18:10:44.320078Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PDFFlatteningService",
                      updatedAt: "2021-02-18T18:10:48.667614Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PDS Product",
                      updatedAt: "2020-12-16T19:27:22.690535Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PDS Product / Plan Deposit Submission (PDS)  Backend",
                      updatedAt: "2021-02-18T18:17:12.485171Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PDS Product / Plan Deposit Submission (PDS) UI",
                      updatedAt: "2020-12-16T14:42:24.491759Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PPR Batch",
                      updatedAt: "2021-02-04T17:57:42.716330Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PPR Database",
                      updatedAt: "2021-02-04T17:57:44.381126Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PPR Product",
                      updatedAt: "2020-12-16T19:26:27.285867Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PPR Product / Personal Property Registry (PPR) Backend",
                      updatedAt: "2021-02-18T18:19:38.429268Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PPR Product / Personal Property Registry (PPR) UI",
                      updatedAt: "2021-02-18T17:13:50.081779Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "PPR Product / Personal Property Registry (PPR) UI / ReDistribition",
                      updatedAt: "2021-02-18T17:13:50.079304Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Print Queues",
                      updatedAt: "2021-02-04T17:57:45.634065Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Printed Output",
                      updatedAt: "2021-02-18T18:11:26.935809Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PromoteReports",
                      updatedAt: "2021-05-20T02:22:24.604570Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RBC FTP Maintenance Task",
                      updatedAt: "2021-05-20T02:22:24.617981Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registration Support service",
                      updatedAt: "2021-02-18T18:11:36.111324Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Relay Email/Fax Server",
                      updatedAt: "2021-02-04T17:57:57.674133Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Remove LiveCycle Originals",
                      updatedAt: "2021-05-20T02:22:24.527548Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Reset WRS",
                      updatedAt: "2021-02-18T18:11:42.122122Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RMS Database",
                      updatedAt: "2021-02-18T16:15:14.248594Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Scheduled FTP Service",
                      updatedAt: "2021-02-18T18:11:44.973051Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Scheduled Print Service",
                      updatedAt: "2021-02-18T18:12:18.147558Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Scheduled Report Service",
                      updatedAt: "2021-02-18T18:13:43.400013Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Smart Forms Transfer (T), Mortgage (M), Caveat (C), Discharge (D)",
                      updatedAt: "2021-02-04T17:58:14.199935Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SMTP2Go Cloud",
                      updatedAt: "2021-02-04T17:58:15.317166Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SQL Reporting System (SSRS)",
                      updatedAt: "2021-02-04T17:58:15.947069Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB)",
                      updatedAt: "2021-02-18T18:14:26.594504Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Digitization",
                      updatedAt: "2021-02-18T18:12:31.498276Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Knowledge Base",
                      updatedAt: "2021-02-18T18:12:34.612474Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Land Titles",
                      updatedAt: "2021-02-18T18:12:37.407824Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Survey",
                      updatedAt: "2021-02-18T18:12:40.528621Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books  (SB) / Task Manager",
                      updatedAt: "2021-02-18T18:12:44.265082Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books Database",
                      updatedAt: "2021-02-18T16:15:31.810153Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Survey Books Update Document Service",
                      updatedAt: "2021-02-18T18:12:47.901397Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SurveyBooks Documents Service",
                      updatedAt: "2021-02-18T18:12:51.214470Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Tax Office Database (TOD)",
                      updatedAt: "2021-02-08T15:03:59.088439Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TCHK Product",
                      updatedAt: "2020-12-16T19:26:45.345882Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TCHK Product / Title Check (TCHK) Backend",
                      updatedAt: "2021-02-18T18:18:59.408516Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TCHK Product / Title Check (TCHK) UI",
                      updatedAt: "2021-02-09T15:25:45.428250Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "tmb-core-ui-addon 3.12.19",
                      updatedAt: "2020-12-14T21:47:44.334088Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR AD",
                      updatedAt: "2021-02-04T17:58:37.814527Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client",
                      updatedAt: "2021-02-18T18:12:54.382362Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "TPR Client / Advanced Account Management System (AAMS) Module",
                      updatedAt: "2021-02-18T18:12:57.470923Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client / Apps",
                      updatedAt: "2021-02-18T18:13:01.336307Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client / Document Tracking",
                      updatedAt: "2021-02-18T18:13:04.418109Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client / Find Sales",
                      updatedAt: "2021-02-18T18:13:08.419587Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "TPR Client / Firms, Users, Security Profile, Settings",
                      updatedAt: "2021-02-18T18:13:11.775424Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "TPR Client / Manitoba Land Titles System (MLTS) Module",
                      updatedAt: "2021-02-18T18:13:15.057242Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Client / Reports",
                      updatedAt: "2021-02-18T18:13:17.861729Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Database",
                      updatedAt: "2021-02-18T16:16:01.225101Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Exports",
                      updatedAt: "2021-02-18T18:13:20.649116Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR PDF Splitter service",
                      updatedAt: "2021-02-18T18:13:23.785570Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Reports/TPR Exports/Update XLSX",
                      updatedAt: "2021-02-18T18:13:27.007555Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Scan and Trigger service",
                      updatedAt: "2021-02-18T18:13:29.656243Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Service",
                      updatedAt: "2021-02-18T18:13:33.593856Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Web SiteTax Office Database (TOD)",
                      updatedAt: "2021-02-04T17:58:53.166584Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TPR Workflow Document service",
                      updatedAt: "2021-02-18T18:13:51.169768Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "TPR's Unified Data Repository Administration (TUNDRA) Module",
                      updatedAt: "2021-02-18T18:14:03.377440Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "tprmb.ca Web Site",
                      updatedAt: "2021-02-04T17:58:57.509874Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Transaction Batch Acceptance Rejection (TBAR)",
                      updatedAt: "2021-02-18T18:14:30.090633Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "UMT - User Management System",
                      updatedAt: "2021-02-04T17:59:00.344501Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Workflow Initialization Service",
                      updatedAt: "2021-05-20T02:22:24.583679Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Workflow Registration Service",
                      updatedAt: "2021-05-20T02:22:24.558769Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "WorkflowWrapperService",
                      updatedAt: "2021-02-18T18:14:40.518414Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Specialized Services - SS",
              factsheets: {
                totalCount: 2,
                edges: [
                  {
                    node: {
                      displayName: "Finance Service 10.5",
                      updatedAt: "2021-06-25T01:58:38.017174Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Marketing",
              factsheets: {
                totalCount: 2,
                edges: [
                  {
                    node: {
                      displayName: "Act-ON",
                      updatedAt: "2020-11-19T20:12:03.418583Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Sprout Social Cloud",
                      updatedAt: "2020-08-13T21:25:42.453999Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Finance Analytics Shared Services",
              factsheets: {
                totalCount: 1,
                edges: [
                  {
                    node: {
                      displayName: "Purview Billing",
                      updatedAt: "2020-12-09T00:18:29.875489Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Sales Operations ",
              factsheets: {
                totalCount: 1,
                edges: [
                  {
                    node: {
                      displayName: "Salesforce Sales Cloud SaaS",
                      updatedAt: "2020-08-18T19:28:29.546450Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Analytics",
              factsheets: {
                totalCount: 16,
                edges: [
                  {
                    node: {
                      displayName: "CDC Framework",
                      updatedAt: "2020-06-18T12:43:02.342585Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CMS Registries Operational Dashboard",
                      updatedAt: "2020-08-12T19:38:36.650022Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Data Lake",
                      updatedAt: "2020-05-14T03:44:14.113184Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ESR - Mapping KPI",
                      updatedAt: "2020-05-14T03:44:14.211102Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ESR - Performance Reporting",
                      updatedAt: "2020-05-14T03:44:14.315322Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - Commercial Revenue Report",
                      updatedAt: "2020-05-14T03:44:14.421984Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - Financial Business Forecasting",
                      updatedAt: "2020-08-12T19:34:21.411259Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - MOR Dashboard",
                      updatedAt: "2020-12-09T00:15:07.403915Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - Registration & Search Model",
                      updatedAt: "2020-05-14T03:44:14.640815Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FI - Weekly Volume Report",
                      updatedAt: "2020-05-14T03:44:14.738284Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS  AVM 2",
                      updatedAt: "2020-05-14T03:44:14.928688Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS CHMC Fraud Detection",
                      updatedAt: "2020-05-14T03:44:15.054217Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS LendView",
                      updatedAt: "2020-08-12T19:43:53.804433Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS OMI",
                      updatedAt: "2020-05-14T03:44:15.144727Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS Teranet HPI",
                      updatedAt: "2020-08-12T19:45:35.373526Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "VAS CS Teranet Insight Report",
                      updatedAt: "2020-08-12T19:44:42.336298Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Central Services",
              factsheets: {
                totalCount: 13,
                edges: [
                  {
                    node: {
                      displayName: "CDS",
                      updatedAt: "2020-06-19T16:44:22.452465Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CMV",
                      updatedAt: "2020-06-19T11:51:53.215464Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CPS",
                      updatedAt: "2020-06-18T00:33:41.309795Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CRS",
                      updatedAt: "2020-11-27T21:41:51.030682Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Elastic Search Service",
                      updatedAt: "2020-06-19T11:53:18.178671Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Hosted Pay Page",
                      updatedAt: "2020-11-19T20:43:30.437891Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "New TSB",
                      updatedAt: "2021-07-07T14:57:37.038242Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "New TSB Cloud",
                      updatedAt: "2021-07-07T14:58:00.382289Z",
                      qualitySeal: "APPROVED",
                    },
                  },
                  {
                    node: {
                      displayName: "PINService",
                      updatedAt: "2020-06-19T12:00:53.035933Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Reusable File Server",
                      updatedAt: "2020-06-07T15:00:21.901265Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Reusable File Server DB",
                      updatedAt: "2020-06-06T19:34:52.262394Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TSB",
                      updatedAt: "2021-07-07T14:57:36.911852Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Billing",
                      updatedAt: "2020-12-08T15:28:11.945336Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "CSP",
              factsheets: {
                totalCount: 14,
                edges: [
                  {
                    node: {
                      displayName: "CSP Framework  ",
                      updatedAt: "2020-06-19T16:57:45.348464Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CSP Framework   / CSP DB  ",
                      updatedAt: "2020-06-06T14:23:32.867699Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LRO Admin  ",
                      updatedAt: "2020-06-05T19:36:49.597983Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "MOF Data Collection  ",
                      updatedAt: "2020-06-08T20:29:28.963071Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnLand   ",
                      updatedAt: "2020-06-19T16:56:53.025018Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnLand    / Onland API  ",
                      updatedAt: "2020-06-05T19:47:01.175420Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnLand    / Onland CMS  ",
                      updatedAt: "2020-06-05T19:52:54.751704Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnLand    / Onland PT  ",
                      updatedAt: "2020-06-05T20:27:47.636018Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OWL  ",
                      updatedAt: "2020-06-06T13:57:24.231199Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Print Manager  ",
                      updatedAt: "2020-06-06T14:04:10.742765Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Rosco",
                      updatedAt: "2020-06-06T14:22:01.375632Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Rosco / Rosco PT  ",
                      updatedAt: "2020-06-19T16:52:57.424556Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Express  ",
                      updatedAt: "2020-06-08T20:26:51.143878Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Express   / Teranet Express PT  ",
                      updatedAt: "2020-06-06T15:03:53.202819Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "DoProcess",
              factsheets: {
                totalCount: 14,
                edges: [
                  {
                    node: {
                      displayName: "Act-ON",
                      updatedAt: "2020-11-19T20:12:03.418583Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "CommHub",
                      updatedAt: "2020-11-19T18:13:52.551174Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Conveyancer",
                      updatedAt: "2020-11-19T18:17:59.553973Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Do Process Wrapper",
                      updatedAt: "2020-06-29T15:58:04.463805Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "E-ComUnity",
                      updatedAt: "2020-06-29T15:59:14.766188Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FastCompany",
                      updatedAt: "2020-11-19T20:04:20.655894Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law",
                      updatedAt: "2020-11-19T21:36:04.810425Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Docgen",
                      updatedAt: "2020-11-19T21:37:27.440947Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity Law API",
                      updatedAt: "2020-12-10T14:20:28.789897Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity Law Connect",
                      updatedAt: "2020-11-19T21:37:39.900534Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity Law DB",
                      updatedAt: "2020-11-19T21:37:51.183418Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity Law Web",
                      updatedAt: "2020-11-19T21:38:05.882129Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Unity Law / Unity PI",
                      updatedAt: "2020-11-19T21:38:21.219318Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "WillBuilder",
                      updatedAt: "2020-11-19T21:38:53.452986Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Ent Systems",
              factsheets: {
                totalCount: 14,
                edges: [
                  {
                    node: {
                      displayName: "Box.com Cloud",
                      updatedAt: "2020-11-19T20:12:49.578689Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Cobblestone Contract Insight 7 (7.0)",
                      updatedAt: "2020-07-06T13:21:18.395588Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "DocuSign Cloud",
                      updatedAt: "2020-06-09T15:16:19.527353Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "FileNet ECM",
                      updatedAt: "2020-06-09T15:20:49.848926Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Kainos Smart Cloud",
                      updatedAt: "2020-12-09T16:41:28.296104Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Pluralsight Cloud",
                      updatedAt: "2020-06-29T16:14:20.996300Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SAP BPC",
                      updatedAt: "2020-06-10T17:44:13.285419Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SAP Business Objects",
                      updatedAt: "2020-06-05T21:34:41.020777Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SAP BW",
                      updatedAt: "2020-06-10T17:46:00.752337Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SAP ECC",
                      updatedAt: "2020-06-10T17:46:59.519047Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ServiceNow Cloud",
                      updatedAt: "2020-12-08T18:46:18.903571Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SharePoint",
                      updatedAt: "2020-10-05T14:51:42.320715Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Workday Cloud",
                      updatedAt: "2020-12-08T18:43:47.084267Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Zendesk Cloud",
                      updatedAt: "2020-06-11T20:31:45.992583Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Geowarehouse",
              factsheets: {
                totalCount: 15,
                edges: [
                  {
                    node: {
                      displayName: "Condo Concentration",
                      updatedAt: "2020-11-19T18:14:06.399333Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse",
                      updatedAt: "2020-11-19T20:02:29.112355Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / eStore",
                      updatedAt: "2020-11-19T18:14:40.893143Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse Api",
                      updatedAt: "2020-11-19T20:08:11.863271Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse CustomerCare",
                      updatedAt: "2020-11-19T20:09:16.544323Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse DB",
                      updatedAt: "2020-12-08T15:06:02.423050Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Geowarehouse / Geowarehouse Management Report",
                      updatedAt: "2020-11-19T20:13:26.938593Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse Product Admin",
                      updatedAt: "2020-11-19T20:13:42.740833Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse Reporting",
                      updatedAt: "2020-11-19T20:08:43.148940Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Geowarehouse UI",
                      updatedAt: "2020-11-19T20:14:32.476144Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / HeatMaps",
                      updatedAt: "2020-11-19T20:17:38.728683Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Geowarehouse / Map Tile Service",
                      updatedAt: "2020-11-19T20:14:13.090856Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Geowarehouse / Map Tile Service / Geomedia WebMap",
                      updatedAt: "2020-11-19T20:18:06.166327Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Google Analytics",
                      updatedAt: "2020-11-19T20:42:12.243645Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "LendView",
                      updatedAt: "2020-12-09T00:14:44.634529Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Mapping",
              factsheets: {
                totalCount: 37,
                edges: [
                  {
                    node: {
                      displayName: "eMap",
                      updatedAt: "2020-06-05T18:43:36.076373Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Copycache",
                      updatedAt: "2020-06-11T13:06:01.622402Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Daily Update",
                      updatedAt: "2020-06-15T14:42:51.778014Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / eMap DB",
                      updatedAt: "2020-06-01T17:40:55.779407Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Health Check",
                      updatedAt: "2020-06-01T17:32:27.833694Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Log Analyzer",
                      updatedAt: "2020-06-11T13:06:32.327506Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Log Archiving",
                      updatedAt: "2020-06-01T17:33:40.751794Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Log Monitor",
                      updatedAt: "2020-06-05T19:13:35.125841Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / PIN Notification",
                      updatedAt: "2020-06-11T13:07:05.074061Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Server & Customizations",
                      updatedAt: "2020-06-15T14:13:36.229052Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "eMap / Service Publisher",
                      updatedAt: "2020-06-11T13:07:31.625813Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap",
                      updatedAt: "2020-06-29T18:16:21.718167Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / AMMR - Automated Map Maintenance Request",
                      updatedAt: "2020-06-11T13:01:04.908183Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / AMMR DB",
                      updatedAt: "2020-05-29T19:05:01.746893Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / CDS Library",
                      updatedAt: "2020-06-01T17:42:39.176413Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Control Panel",
                      updatedAt: "2020-06-05T19:27:43.139259Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Desktop & Addins",
                      updatedAt: "2020-06-11T13:12:57.313680Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Enterprise Geospatial",
                      updatedAt: "2020-12-10T20:10:03.253415Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / Enterprise Geospatial / Delivery Manager",
                      updatedAt: "2020-12-10T20:10:03.252346Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / Enterprise Geospatial / Delivery SDO DB",
                      updatedAt: "2020-12-10T20:10:03.248757Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / Enterprise Geospatial / Delivery ST DB",
                      updatedAt: "2020-12-10T20:10:03.240896Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Enterprise Geospatial / Dengine",
                      updatedAt: "2020-12-10T20:10:03.256500Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / Enterprise Geospatial / Tile Generation",
                      updatedAt: "2020-12-10T20:10:03.244942Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Get Image",
                      updatedAt: "2020-06-11T13:03:37.102539Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / JPS - Job Processing Service",
                      updatedAt: "2020-06-11T13:04:08.274063Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Legal Text Parser",
                      updatedAt: "2020-05-29T19:35:45.739589Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Maintenance",
                      updatedAt: "2020-11-24T14:13:14.624467Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Mapping Title Update",
                      updatedAt: "2020-06-11T13:04:57.319581Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Monitoring",
                      updatedAt: "2020-06-05T18:18:42.323459Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "OnMap / MTMS - Mapping Task Management System",
                      updatedAt: "2020-05-29T19:37:52.406614Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / MTMS DB",
                      updatedAt: "2020-05-29T19:39:23.587883Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / OnMap DB",
                      updatedAt: "2020-05-29T19:46:00.704315Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / PAT - Parcel Automation Tool",
                      updatedAt: "2020-06-05T18:18:07.754Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / QMS - Queue Management System",
                      updatedAt: "2020-06-05T18:23:49.197774Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Task Manager",
                      updatedAt: "2020-06-11T13:05:28.661515Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OnMap / Title Data Warehouse DB",
                      updatedAt: "2020-05-29T19:53:37.573184Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OpenLDAP .Net Library",
                      updatedAt: "2020-06-12T12:02:22.429825Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "POLARIS",
              factsheets: {
                totalCount: 11,
                edges: [
                  {
                    node: {
                      displayName: "Polaris II",
                      updatedAt: "2020-06-16T20:46:27.631257Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS BT",
                      updatedAt: "2020-06-12T14:31:31.868345Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS DB",
                      updatedAt: "2020-06-12T14:39:44.924919Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS PT",
                      updatedAt: "2020-06-12T14:24:59.106229Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS Reports",
                      updatedAt: "2020-06-17T20:22:06.763202Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / POLARIS Title Update",
                      updatedAt: "2020-06-17T14:59:54.208816Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS",
                      updatedAt: "2020-06-19T16:44:22.464520Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS / RTS BT",
                      updatedAt: "2020-06-17T20:21:44.604979Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS / RTS DB",
                      updatedAt: "2020-06-16T20:42:16.166815Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS / RTS Desktop",
                      updatedAt: "2020-06-16T20:41:17.330922Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Polaris II / RTS / RTS PT",
                      updatedAt: "2020-06-16T20:20:50.276302Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Purview",
              factsheets: {
                totalCount: 11,
                edges: [
                  {
                    node: {
                      displayName: "Pin Monitoring",
                      updatedAt: "2020-11-19T20:45:07.636259Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview",
                      updatedAt: "2020-12-10T19:47:43.859262Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / DB",
                      updatedAt: "2020-11-19T20:46:28.003658Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / Purview Application",
                      updatedAt: "2020-11-19T20:46:45.088114Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / Purview Reporting",
                      updatedAt: "2020-11-19T20:47:24.120169Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / Reavs",
                      updatedAt: "2020-11-19T21:26:51.157534Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview / Reavs / Reavs DB",
                      updatedAt: "2020-11-19T20:47:39.481504Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RedX",
                      updatedAt: "2020-11-19T21:29:01.072519Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RedX / RedX DB",
                      updatedAt: "2020-11-19T21:29:18.414797Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RedX / RedX Desktop",
                      updatedAt: "2020-11-19T21:29:33.183518Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RedX / RedX Web",
                      updatedAt: "2020-11-19T21:29:48.071219Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Recovery",
              factsheets: {
                totalCount: 184,
                edges: [
                  {
                    node: {
                      displayName: "Recovery",
                      updatedAt: "2021-01-20T19:11:52.638390Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Account Maintenance Service (Recovery Services)",
                      updatedAt: "2021-04-29T03:11:00.001342Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Active_Download_vs2008",
                      updatedAt: "2021-04-29T03:10:59.995666Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AspState Database",
                      updatedAt: "2021-04-29T03:10:59.975750Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset Admin",
                      updatedAt: "2021-04-29T03:10:59.981675Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset Collection",
                      updatedAt: "2021-04-29T03:10:59.963759Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset Registry",
                      updatedAt: "2020-06-09T20:09:57.835012Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset Reporting",
                      updatedAt: "2020-06-29T19:15:13.299118Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset RMS",
                      updatedAt: "2020-06-05T02:59:50.617308Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset RMS Web Services",
                      updatedAt: "2020-06-29T19:15:13.700445Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.BankruptcyDailyWorklist",
                      updatedAt: "2021-04-21T01:43:37.877777Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.BNSInvalidPhoneLoad",
                      updatedAt: "2021-04-21T01:43:37.895464Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.BNSScorecard",
                      updatedAt: "2021-04-21T01:43:37.884677Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.CACSCloseout",
                      updatedAt: "2020-11-03T13:15:51.877070Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.DailyUserPerformances",
                      updatedAt: "2021-04-21T01:43:37.910436Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.FifthCycleBillingAutoTrigger",
                      updatedAt: "2020-06-29T19:15:16.174229Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.File.Upload.Validation",
                      updatedAt: "2020-06-29T19:15:16.636957Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.MOCBatchQ",
                      updatedAt: "2020-11-03T13:12:47.519130Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.RBCBOCLoad",
                      updatedAt: "2020-11-03T13:14:28.319954Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Asset.Batch.RBCPreAuthorizedPayment",
                      updatedAt: "2021-04-21T01:43:37.900538Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.ShawResponseTrigger",
                      updatedAt: "2020-06-29T19:15:18.334507Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Asset.Batch.TrendMonthly",
                      updatedAt: "2020-06-29T19:15:18.774672Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetAdmin Database",
                      updatedAt: "2020-06-05T02:44:29.947891Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetAudit Database",
                      updatedAt: "2020-06-12T20:52:54.368235Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetFax Database",
                      updatedAt: "2020-06-12T20:53:22.046871Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetFiles Database",
                      updatedAt: "2020-06-12T20:53:47.219655Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AssetSecurity Database",
                      updatedAt: "2020-06-12T20:54:09.896942Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ASSETWorkTrace",
                      updatedAt: "2021-04-29T03:11:00.014648Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / AutoQNationalSales",
                      updatedAt: "2020-06-29T19:15:19.773126Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Bankrupt Account Setup Validation Service (Recovery Services)",
                      updatedAt: "2020-06-29T19:15:20.199379Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Bankruptcy Database",
                      updatedAt: "2020-06-09T20:12:34.528790Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Bankruptcy EXE",
                      updatedAt: "2020-06-29T19:15:20.648271Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Bankruptcy Highway",
                      updatedAt: "2020-06-29T19:15:21.091552Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BankruptcyAdmin Database",
                      updatedAt: "2020-06-05T02:44:50.343818Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BankruptcyAudit Database",
                      updatedAt: "2020-06-09T20:11:48.248623Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BH_PostBatch",
                      updatedAt: "2020-06-29T19:15:21.544505Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BHLoanNumberFix",
                      updatedAt: "2020-06-29T19:15:21.980441Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BHUserSynchronize",
                      updatedAt: "2020-06-29T19:15:22.428564Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Billing",
                      updatedAt: "2020-06-29T19:15:22.909262Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BKNoTaskScan",
                      updatedAt: "2020-06-29T19:15:23.393452Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BlackBookUpload",
                      updatedAt: "2020-06-03T22:40:17.235011Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BMO_Export_MC_File",
                      updatedAt: "2020-06-03T20:11:23.306832Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BmoDraft5FileExport",
                      updatedAt: "2020-06-03T20:12:46.710551Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BmoFileExport",
                      updatedAt: "2020-06-05T03:12:26.409994Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BmoFileImport",
                      updatedAt: "2020-06-03T20:15:46.595294Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BNS_Contingency",
                      updatedAt: "2020-06-04T01:35:22.741598Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BNSDebtManager2007App",
                      updatedAt: "2020-06-29T19:15:23.867586Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BNSPaymentApp",
                      updatedAt: "2020-06-05T03:12:43.461295Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BNSRedistributionBatch",
                      updatedAt: "2020-06-03T22:39:56.907234Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / BountyHunter Database",
                      updatedAt: "2020-06-03T20:34:18.760802Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CCSBatch_GL",
                      updatedAt: "2020-06-03T20:40:34.736581Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CCSBatch_Tasks",
                      updatedAt: "2020-06-03T22:40:35.824422Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CCSBOCOverpaymentComplete",
                      updatedAt: "2020-06-04T01:35:55.623014Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CH Database",
                      updatedAt: "2020-06-05T02:59:10.741975Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CH_AccruedInterest",
                      updatedAt: "2020-06-04T01:37:34.815390Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CH_JudgmentAcrIntrst",
                      updatedAt: "2020-06-03T20:58:06.069527Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / ChAgencyAssignmentPostChargeOffCalculation",
                      updatedAt: "2020-06-29T19:15:27.487856Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CHAgencyUpload",
                      updatedAt: "2020-06-29T19:15:27.959265Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CHAGenericFileImport",
                      updatedAt: "2020-06-03T22:42:48.363324Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CHTTAdmin Database",
                      updatedAt: "2020-06-05T02:46:07.917895Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CIBC Database",
                      updatedAt: "2020-06-05T02:57:30.212166Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CIBC.Link.Security",
                      updatedAt: "2020-06-03T22:42:31.075835Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CIBC_Batch",
                      updatedAt: "2020-06-03T21:13:56.932610Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CleanListBatchApp",
                      updatedAt: "2020-06-03T22:42:13.624515Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ClientPrimeIntRate",
                      updatedAt: "2020-06-03T21:30:09.224820Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ClientReconcilationReport",
                      updatedAt: "2020-06-29T19:15:28.447007Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ClientUpdateManager Database",
                      updatedAt: "2020-06-12T20:54:45.066646Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CMS_DEV_COGNOS_102 Database",
                      updatedAt: "2020-06-12T20:55:07.521280Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Cognos",
                      updatedAt: "2020-06-29T19:15:28.923651Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Collection Highway",
                      updatedAt: "2020-06-03T22:41:55.166205Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Collection Services Api Service (Recovery Services)",
                      updatedAt: "2020-06-04T02:34:58.054597Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CopyFileUP",
                      updatedAt: "2020-06-04T01:38:13.639222Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Credit Beaureu Service (Recovery External Services)",
                      updatedAt: "2020-06-04T23:21:41.178238Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / CreditBureauReportFileExport",
                      updatedAt: "2020-06-03T22:41:31.906119Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Critical.Batch",
                      updatedAt: "2020-06-03T22:41:13.237884Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Crystal Reports",
                      updatedAt: "2020-06-29T19:15:29.439099Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DBA Database",
                      updatedAt: "2020-06-12T20:55:26.933694Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DepositBatch",
                      updatedAt: "2020-06-03T22:10:52.528421Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH BlackBook Service (Recovery External Services)",
                      updatedAt: "2020-06-03T22:11:33.908556Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.AssignmentScoreFileImportAndExport",
                      updatedAt: "2021-04-22T02:50:45.776674Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.BNSDebtManagerFileImport",
                      updatedAt: "2021-04-22T02:50:45.767464Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.BNSOutputFiles",
                      updatedAt: "2021-04-22T02:50:45.748918Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DH.RecoveryServices.CACSImport",
                      updatedAt: "2021-04-22T02:50:45.787996Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.CentralizedFileImporter",
                      updatedAt: "2021-04-22T02:50:45.782449Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.DataConversionReports",
                      updatedAt: "2021-04-22T02:50:45.758683Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.DirectPaymentAutoRemit_DH",
                      updatedAt: "2021-01-21T11:48:18.449201Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.DirectPaymentPosting",
                      updatedAt: "2021-04-22T02:50:45.792287Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.Galaxy.AgencyCodeAssignment",
                      updatedAt: "2020-06-04T02:20:02.941338Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.GSLACDebtmanager",
                      updatedAt: "2020-06-04T02:21:48.002459Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.HondaFileImport",
                      updatedAt: "2020-06-04T02:25:38.179459Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.HSBCFileImport",
                      updatedAt: "2020-06-04T02:28:43.434539Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.HSBCNotesAndDataBackup",
                      updatedAt: "2020-06-04T02:31:13.151478Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RBCAutoClosure",
                      updatedAt: "2020-06-04T02:33:03.811037Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RBCDirectPaymentPosting",
                      updatedAt: "2021-04-21T01:43:37.892220Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RBCPapPosting",
                      updatedAt: "2021-04-21T01:43:37.890216Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RBCResolveFileImport",
                      updatedAt: "2021-04-21T01:43:37.908726Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.Redistribution",
                      updatedAt: "2020-06-04T02:42:05.890745Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.RepoDepoInventoryExport",
                      updatedAt: "2020-06-04T02:49:55.077964Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.TDAFAssignmentFileToMDB",
                      updatedAt: "2020-06-04T02:51:54.331279Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.TDAFFileImport",
                      updatedAt: "2020-06-04T02:54:06.181912Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / DH.RecoveryServices.TDAFLegacyAssignmentFiletoMDB",
                      updatedAt: "2020-06-04T02:55:24.341778Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DH.RecoveryServices.TS2Import",
                      updatedAt: "2020-06-04T02:57:10.689506Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Direct Payment Post File Service (Recovery Services)",
                      updatedAt: "2020-06-29T19:15:29.940834Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DirectPaymentAutoProcessBatch",
                      updatedAt: "2020-06-03T22:18:23.040427Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / DirectPaymentAutoRemit",
                      updatedAt: "2020-06-03T22:19:53.156983Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Document Highway",
                      updatedAt: "2020-06-03T22:40:54.463956Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Equipment Database",
                      updatedAt: "2020-06-12T20:56:11.932709Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ExtinguishedDebt",
                      updatedAt: "2020-06-03T22:31:43.460957Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / File Transfer Service",
                      updatedAt: "2020-06-05T02:54:50.039901Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / GlobalDataStore Database",
                      updatedAt: "2020-06-12T20:56:32.426259Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / GLPost",
                      updatedAt: "2020-06-03T22:47:15.140679Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Hangfire Database",
                      updatedAt: "2020-06-05T02:37:47.010091Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / HSBC_Monthly_DataFeed",
                      updatedAt: "2020-06-03T22:53:26.242313Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ICAPRSL Database",
                      updatedAt: "2020-06-12T20:56:52.400158Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Insurance Highway",
                      updatedAt: "2020-06-03T22:59:11.986811Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Letter Service (Recovery Services)",
                      updatedAt: "2020-06-03T23:04:10.189979Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / LetterBatch",
                      updatedAt: "2020-06-03T23:02:18.731128Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / LH Database",
                      updatedAt: "2020-06-05T02:59:37.572127Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Litigation Highway",
                      updatedAt: "2020-06-03T23:12:12.793377Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / Litigation Services Api Service (Recovery Services)",
                      updatedAt: "2020-06-03T23:14:14.023114Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / LogInterestRate",
                      updatedAt: "2020-06-05T03:13:00.064149Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / New_Download_vs2008",
                      updatedAt: "2020-06-03T23:41:44.553113Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / NightlyBatch.AutoXBF",
                      updatedAt: "2020-06-29T19:15:30.401776Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / NightlyBatch.SalesTaskMaintenance",
                      updatedAt: "2020-06-03T23:44:24.376368Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / NightlyBatchThunder",
                      updatedAt: "2020-06-03T23:49:39.320534Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Optimus Portal",
                      updatedAt: "2020-06-29T19:15:30.980865Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Optimus Web Services",
                      updatedAt: "2020-06-05T02:18:39.066866Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Optimus Windows Service",
                      updatedAt: "2020-06-03T23:56:27.771212Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / OutstandingAsgn",
                      updatedAt: "2020-06-04T00:02:19.192599Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Payment Highway",
                      updatedAt: "2020-06-04T00:06:12.763073Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PH_SLInvoiceMiluGenerator",
                      updatedAt: "2020-06-04T00:08:15.767058Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PPSA",
                      updatedAt: "2020-06-29T19:15:31.456701Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PPSA Database",
                      updatedAt: "2020-06-09T20:12:38.964202Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PPSAAdmin Database",
                      updatedAt: "2020-06-05T02:46:57.068045Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / PrimeRateChangeLowestInterest",
                      updatedAt: "2020-06-29T19:15:31.961675Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Property Highway",
                      updatedAt: "2020-06-29T19:15:32.450113Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RBC_Intmast",
                      updatedAt: "2020-06-04T01:07:23.702470Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RBC_Redistribution",
                      updatedAt: "2020-06-04T01:08:32.423767Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RbcFileExport",
                      updatedAt: "2020-06-04T01:09:50.740401Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RBCGLBatch_Process",
                      updatedAt: "2020-06-04T01:11:02.991924Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RBCIThirdPartyLoad",
                      updatedAt: "2020-06-04T01:12:56.776549Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Rdstrbtn_CleanUP",
                      updatedAt: "2020-06-04T01:14:10.531483Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RecalcColInvPerDiem",
                      updatedAt: "2020-06-04T01:15:25.895306Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RecalcPerDiemJudgmnt",
                      updatedAt: "2020-06-04T01:17:45.112202Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Recovery Hangfire",
                      updatedAt: "2020-06-09T19:40:26.363956Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Redistribution",
                      updatedAt: "2020-06-04T17:09:58.240152Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Registries Online",
                      updatedAt: "2020-06-04T23:16:38.748105Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Repo Highway",
                      updatedAt: "2020-06-04T23:21:53.093075Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RepoDepo Database",
                      updatedAt: "2020-06-29T19:15:32.966137Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Report Viewer",
                      updatedAt: "2020-06-29T19:15:33.475604Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / ReportLog Service (Asset Audit International Services)",
                      updatedAt: "2020-06-04T23:25:03.784076Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / ReportLog Service (Asset Audit Services)",
                      updatedAt: "2020-06-04T23:58:08.447074Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / ReportServer Database",
                      updatedAt: "2020-06-29T19:15:33.943696Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RevokeStatus",
                      updatedAt: "2020-06-04T23:29:45.707352Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / RH Audit Log Service (Asset Audit International Services)",
                      updatedAt: "2020-06-04T23:31:11.805305Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / RH Audit Log Service (Asset Audit Services)",
                      updatedAt: "2020-06-04T23:33:33.709541Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RHCostRecoveryGLInvoice",
                      updatedAt: "2020-06-04T23:43:29.869460Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMS Db",
                      updatedAt: "2020-06-29T19:15:34.450287Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMS2 Database",
                      updatedAt: "2020-06-29T19:15:34.965660Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMSABDemandRelocateScan",
                      updatedAt: "2020-06-04T23:50:43.036105Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMSNOTES Database",
                      updatedAt: "2020-06-29T19:15:35.462220Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMSTaskMissing",
                      updatedAt: "2020-06-04T23:52:23.321898Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / RMSWI Database",
                      updatedAt: "2020-06-29T19:15:35.944678Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SDALegacyLoad",
                      updatedAt: "2020-06-04T23:54:09.043504Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SetlementStatus",
                      updatedAt: "2020-06-04T23:56:01.040901Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Skip Locator",
                      updatedAt: "2020-06-04T23:59:03.139542Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Skip_NightBatch",
                      updatedAt: "2020-06-05T00:02:47.654150Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipBankReview",
                      updatedAt: "2020-06-05T02:04:00.202104Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipLocator Database",
                      updatedAt: "2020-06-29T19:15:36.461559Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipLocatorAdmin Database",
                      updatedAt: "2020-06-29T19:15:36.960154Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipLocatorDTS",
                      updatedAt: "2020-06-05T02:05:23.301845Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SkipNewAssignment",
                      updatedAt: "2020-06-05T02:09:32.833949Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SRQ",
                      updatedAt: "2020-06-05T02:11:41.454289Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SRQ Database",
                      updatedAt: "2020-06-29T19:15:37.467271Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / SSRS Reports Server",
                      updatedAt: "2020-06-05T02:12:45.864106Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / Statement Generation Service",
                      updatedAt: "2020-06-05T02:16:18.039067Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / StatementBatch",
                      updatedAt: "2020-06-05T02:17:29.101588Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / TaskService Database",
                      updatedAt: "2020-06-29T19:15:37.993253Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / TravelersAssignments",
                      updatedAt: "2020-06-05T02:18:47.408227Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / TravelersResponse",
                      updatedAt: "2020-06-05T02:19:42.582077Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / TyMetrixBatch",
                      updatedAt: "2020-06-05T02:20:36.879760Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / UserActivity Service (Asset Audit International Services)",
                      updatedAt: "2020-06-05T02:23:09.944918Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Recovery / UserActivity Service (Asset Audit Services)",
                      updatedAt: "2020-06-05T02:25:17.360926Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Recovery / WorkflowAutomation",
                      updatedAt: "2020-06-05T02:29:34.650753Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "SecOps",
              factsheets: {
                totalCount: 18,
                edges: [
                  {
                    node: {
                      displayName: "Cofense PhishMe Cloud",
                      updatedAt: "2020-07-03T15:08:53.714144Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Digital Guardian DLP Cloud",
                      updatedAt: "2020-07-03T15:09:54.308154Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Entrust",
                      updatedAt: "2020-07-08T18:18:54.586172Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "F5 Networks BIG-IP Application Security Manager (ASm)",
                      updatedAt: "2020-06-17T16:06:11.969582Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "F5 Networks Silverline DDoS Cloud",
                      updatedAt: "2020-07-03T15:10:45.071551Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Gemalto SafeNet HSM Client",
                      updatedAt: "2020-05-14T04:29:29.961225Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Imperva SecureSphere",
                      updatedAt: "2020-07-21T03:07:09.344542Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Azure Active Directory Cloud",
                      updatedAt: "2020-07-03T18:34:41.063940Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Nessus Security Center",
                      updatedAt: "2020-07-08T18:36:00.007365Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Okta Identity Cloud",
                      updatedAt: "2020-07-03T15:12:37.800489Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OpenLDAP",
                      updatedAt: "2020-07-08T18:40:34.326809Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Prisma Cloud Compute",
                      updatedAt: "2020-11-03T20:39:09.776257Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Private Ark Password Vault",
                      updatedAt: "2020-07-08T18:39:54.598561Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Rapid7 Suit Cloud",
                      updatedAt: "2020-06-17T03:49:26.291207Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RSA SecurID Authentication",
                      updatedAt: "2020-12-16T15:16:15.910434Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SANS Security Awareness Cloud",
                      updatedAt: "2020-07-03T15:15:01.894414Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Symantec Antivirus",
                      updatedAt: "2020-07-21T03:08:24.548592Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Symantec SEP",
                      updatedAt: "2020-06-04T14:23:37.050322Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Teraview",
              factsheets: {
                totalCount: 42,
                edges: [
                  {
                    node: {
                      displayName: "OpenLDAP Library",
                      updatedAt: "2020-07-06T18:55:41.184475Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview",
                      updatedAt: "2021-06-23T15:26:16.182491Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Blob Repository",
                      updatedAt: "2020-11-10T22:20:37.337594Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Blob Sync",
                      updatedAt: "2020-06-16T20:33:44.258098Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Bookloading",
                      updatedAt: "2020-06-16T20:35:03.340746Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Bulk Reporting",
                      updatedAt: "2020-06-16T20:35:28.828872Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / CAS",
                      updatedAt: "2020-12-16T15:16:15.898250Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / CSC Teradmin PT",
                      updatedAt: "2020-09-30T14:08:54.451703Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Database AppBatch Jobs",
                      updatedAt: "2020-06-05T19:51:11.136904Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / DAWS",
                      updatedAt: "2021-06-23T15:26:20.682065Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / EFT",
                      updatedAt: "2020-06-16T20:37:29.849691Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / GL",
                      updatedAt: "2020-06-16T20:37:09.658339Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Image Upload",
                      updatedAt: "2020-07-28T13:59:30.402858Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / jBlobsvr",
                      updatedAt: "2020-07-14T14:54:08.198957Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / jCert",
                      updatedAt: "2020-06-16T20:36:18.403674Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Teraview / Long Term Signature Verification",
                      updatedAt: "2020-06-16T20:43:19.184349Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Lsuc/AOLS Scripts",
                      updatedAt: "2020-06-05T22:03:49.471049Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / LTTSXML",
                      updatedAt: "2020-06-19T15:35:43.317032Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Management Report",
                      updatedAt: "2020-06-05T20:59:41.236378Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / MyTeranetConnect",
                      updatedAt: "2021-06-23T15:26:24.373446Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / NGTV BRMS",
                      updatedAt: "2020-06-05T22:20:15.347897Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / NGTV BRMS DB",
                      updatedAt: "2020-06-05T22:18:02.465253Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / NGTV Portas",
                      updatedAt: "2021-02-04T19:59:12.359708Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Pass DB",
                      updatedAt: "2020-12-11T18:45:58.213691Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Password Change Scripts",
                      updatedAt: "2020-06-05T21:19:34.606928Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / PortasAdmin",
                      updatedAt: "2020-06-16T20:41:55.104565Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Teraview / Recovery - Pending Registrations",
                      updatedAt: "2020-06-05T21:25:25.936951Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Request Processor",
                      updatedAt: "2020-12-04T16:58:58.686187Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Server Monitor",
                      updatedAt: "2020-06-16T20:41:12.112878Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / TD Diamond",
                      updatedAt: "2020-06-16T20:40:54.865622Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teradmin BT",
                      updatedAt: "2020-06-16T20:40:31.154396Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Audit Archived DB",
                      updatedAt: "2020-12-11T18:46:00.599998Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Billing DB",
                      updatedAt: "2020-06-05T21:44:25.586306Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Central DB",
                      updatedAt: "2020-06-08T15:27:46.433996Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Ltts DB",
                      updatedAt: "2020-12-11T18:46:03.637012Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Report DB",
                      updatedAt: "2020-12-11T18:46:16.464448Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Support Portal",
                      updatedAt: "2020-06-06T01:17:52.809186Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Web BT",
                      updatedAt: "2020-07-28T13:58:13.195237Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Web eReg PT",
                      updatedAt: "2020-06-19T15:34:02.412305Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview Web LRO PT",
                      updatedAt: "2020-06-06T01:20:56.992075Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / Teraview.ca",
                      updatedAt: "2020-06-15T16:32:59.408184Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teraview / WEF Portas",
                      updatedAt: "2021-05-05T19:52:12.387678Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "SDLC",
              factsheets: {
                totalCount: 22,
                edges: [
                  {
                    node: {
                      displayName: "Aha! Cloud",
                      updatedAt: "2020-07-13T15:09:07.387905Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ALMQC",
                      updatedAt: "2020-06-17T13:32:40.780457Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Artifactory",
                      updatedAt: "2020-07-13T18:16:48.479737Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Atlassian Bitbucket",
                      updatedAt: "2020-12-11T19:20:33.540143Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Atlassian Confluence",
                      updatedAt: "2020-12-11T19:12:43.871277Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Atlassian JIRA",
                      updatedAt: "2020-12-11T19:24:39.630880Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Beyond Compare",
                      updatedAt: "2020-12-11T20:39:42.076515Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Blueprint Storyteller",
                      updatedAt: "2020-06-17T13:42:25.589832Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "ConnectALL Integration Platform",
                      updatedAt: "2020-06-17T13:34:30.986197Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Dependency Checker",
                      updatedAt: "2020-05-14T04:15:27.678167Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Gerrit",
                      updatedAt: "2020-06-17T13:12:50.500673Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Git",
                      updatedAt: "2020-06-17T13:20:23.570021Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "JAWS",
                      updatedAt: "2020-05-14T04:15:28.236943Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Jenkins CI",
                      updatedAt: "2020-07-13T18:22:21.067959Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Micro Focus Fortify",
                      updatedAt: "2020-07-20T17:59:48.710559Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "OWASP ZAP",
                      updatedAt: "2020-05-14T04:15:28.827986Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Resharper Ultimate",
                      updatedAt: "2020-12-11T20:53:16.253974Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Selenium Framework",
                      updatedAt: "2021-02-18T18:23:52.239288Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SmartBear ReadyAPI!",
                      updatedAt: "2020-05-14T04:15:29.415685Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SmartGit",
                      updatedAt: "2020-06-17T13:25:34.847906Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Sparx Enterprise Architect",
                      updatedAt: "2020-05-14T04:15:30.007601Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "XMLSpy",
                      updatedAt: "2020-05-14T04:15:30.296079Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Writs",
              factsheets: {
                totalCount: 13,
                edges: [
                  {
                    node: {
                      displayName: "Teranet Express   / WEF  ",
                      updatedAt: "2020-06-06T15:11:41.908462Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs",
                      updatedAt: "2020-06-07T15:05:47.528752Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / New WEF",
                      updatedAt: "2020-06-07T15:13:12.670386Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / New WEF DB",
                      updatedAt: "2020-06-06T19:39:15.644006Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Remote Access Service (RAAC)",
                      updatedAt: "2020-06-07T15:02:39.770553Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / ReportGen",
                      updatedAt: "2020-06-06T19:28:33.226166Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / WDF",
                      updatedAt: "2020-06-06T19:27:59.321582Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / WDF DB",
                      updatedAt: "2020-06-06T19:33:17.090776Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs BT",
                      updatedAt: "2020-06-06T17:25:34.686215Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs DB",
                      updatedAt: "2020-12-08T15:12:28.958234Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs Desktop",
                      updatedAt: "2020-06-06T19:27:02.181003Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs NGBT",
                      updatedAt: "2020-06-07T19:45:49.333628Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Writs / Writs PT",
                      updatedAt: "2020-06-06T19:23:15.193952Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Infrastructure",
              factsheets: {
                totalCount: 9,
                edges: [
                  {
                    node: {
                      displayName: "Active Directory",
                      updatedAt: "2021-02-04T17:51:47.076393Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Bradmark Surveillance",
                      updatedAt: "2020-07-08T18:05:32.432648Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Elastic Stack",
                      updatedAt: "2020-07-08T18:22:04.289156Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Active Directory 2012",
                      updatedAt: "2020-07-03T18:37:08.638541Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft SCCM",
                      updatedAt: "2020-07-14T19:10:28.387366Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Skype for Business",
                      updatedAt: "2020-07-08T18:33:56.039664Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft Teams",
                      updatedAt: "2020-07-14T19:14:00.022953Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "SolarWinds Orion",
                      updatedAt: "2020-07-14T13:28:18.071075Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Zoom Cloud",
                      updatedAt: "2020-08-23T16:31:07.368212Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Finance",
              factsheets: {
                totalCount: 2,
                edges: [
                  {
                    node: {
                      displayName:
                        "BlackLine Unified Cloud for Accounting and Finance",
                      updatedAt: "2020-08-18T19:28:05.426845Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Purview Billing",
                      updatedAt: "2020-12-09T00:18:29.875489Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "TX",
              factsheets: {
                totalCount: 7,
                edges: [
                  {
                    node: {
                      displayName: "Teranet Xchange",
                      updatedAt: "2020-11-19T21:31:33.656803Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / Teranet Xchange Batch",
                      updatedAt: "2020-11-19T21:31:47.335035Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / Teranet Xchange DB",
                      updatedAt: "2020-11-19T21:32:05.044232Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / Teranet Xchange Online",
                      updatedAt: "2020-11-19T21:32:22.327608Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / Teranet Xchange Web",
                      updatedAt: "2020-11-19T21:32:55.850371Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / TeranetXchange OnDemand",
                      updatedAt: "2020-11-19T21:33:06.877106Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Teranet Xchange / TeranetXchange TCopy",
                      updatedAt: "2020-11-19T21:33:36.672149Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "CGe (Collateral Guard Enterprise)",
              factsheets: {
                totalCount: 13,
                edges: [
                  {
                    node: {
                      displayName: "Collateral Guard Enterprise (CGe)",
                      updatedAt: "2021-04-18T21:09:49.859305Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE Database",
                      updatedAt: "2020-06-09T18:47:25.182207Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE Hangfire",
                      updatedAt: "2021-02-23T11:41:35.909623Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE REST API (Internal)",
                      updatedAt: "2021-06-16T01:02:53.505488Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE Web Services",
                      updatedAt: "2021-02-23T11:43:13.156390Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE_Archive Database",
                      updatedAt: "2020-06-09T18:50:27.766277Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE_Audit Database",
                      updatedAt: "2020-06-09T18:51:01.380138Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE_Docs Database",
                      updatedAt: "2020-06-09T18:51:39.134567Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGE_Logs Database",
                      updatedAt: "2020-06-09T18:54:18.270681Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / Collateral Guard Enterprise (CGE) Web",
                      updatedAt: "2021-02-23T11:43:59.236421Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / PDF2Data Database",
                      updatedAt: "2020-11-10T16:52:35.126135Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / Pdf2DataAPI",
                      updatedAt: "2020-11-10T16:52:53.649650Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "RDPRM",
                      updatedAt: "2020-10-19T20:03:03.981065Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "CGe CD (Collateral Guard Enterprise Client Delivery)",
              factsheets: {
                totalCount: 10,
                edges: [
                  {
                    node: {
                      displayName: "Collateral Guard Enterprise (CGe) / CGe CD",
                      updatedAt: "2020-06-29T19:15:27.021283Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Admin Dashboard Web",
                      updatedAt: "2020-09-11T13:22:51.707341Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD API Database",
                      updatedAt: "2020-09-11T13:23:41.987173Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Batch Console",
                      updatedAt: "2020-09-11T13:23:14.144541Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Batch Database",
                      updatedAt: "2020-09-11T13:23:53.733209Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Data Migration Process",
                      updatedAt: "2020-09-11T13:24:47.594257Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD LVS Report",
                      updatedAt: "2020-09-11T13:24:57.045276Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD Middleware Dashboard",
                      updatedAt: "2020-09-08T13:09:47.554878Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD REST API",
                      updatedAt: "2020-12-11T21:28:16.802936Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Collateral Guard Enterprise (CGe) / CGe CD / CGe CD SOAP API",
                      updatedAt: "2020-09-11T13:25:14.578354Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Registry CD (Client Delivery)",
              factsheets: {
                totalCount: 40,
                edges: [
                  {
                    node: {
                      displayName: "Registry CD",
                      updatedAt: "2020-06-29T19:15:26.666392Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD BAS",
                      updatedAt: "2020-06-29T19:15:23.891303Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD BBY CSRS RPA",
                      updatedAt: "2020-06-29T19:15:23.918709Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD DataLake",
                      updatedAt: "2020-06-29T19:15:23.953273Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC",
                      updatedAt: "2020-06-29T19:15:24.207954Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC Admin Web",
                      updatedAt: "2020-06-29T19:15:23.994290Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD KYC / CD KYC Automation GUI",
                      updatedAt: "2020-06-29T19:15:24.023781Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC Client Web",
                      updatedAt: "2020-06-29T19:15:24.057575Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC Database",
                      updatedAt: "2020-06-29T19:15:24.090495Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC REST API",
                      updatedAt: "2020-06-29T19:15:24.126378Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD KYC / CD KYC Service Scheduler",
                      updatedAt: "2020-06-29T19:15:24.164193Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD KYC / CD KYC Web Service",
                      updatedAt: "2020-06-29T19:15:24.206016Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy BBY CSRS Interanet Web (JSP)",
                      updatedAt: "2020-06-29T19:15:24.275624Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy BBY CSRS Web (ASPX)",
                      updatedAt: "2020-06-29T19:15:24.329960Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY Database",
                      updatedAt: "2020-06-29T19:15:24.387607Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY DOSPPSA",
                      updatedAt: "2020-06-29T19:15:24.440747Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy BBY File Dispatcher",
                      updatedAt: "2020-06-29T19:15:24.501759Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY HOST",
                      updatedAt: "2020-06-29T19:15:24.565656Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY Reports",
                      updatedAt: "2020-06-29T19:15:24.632380Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY WinBill",
                      updatedAt: "2020-06-29T19:15:24.702517Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy BBY WINPPSA",
                      updatedAt: "2020-06-29T19:15:24.786102Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy ClearCharge",
                      updatedAt: "2020-06-29T19:15:24.866788Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Admin Web",
                      updatedAt: "2020-06-29T19:15:24.941257Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Automation GUI",
                      updatedAt: "2020-06-29T19:15:25.022509Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Batch Console",
                      updatedAt: "2020-06-29T19:15:25.108930Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Client Web",
                      updatedAt: "2020-06-29T19:15:25.198578Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Database",
                      updatedAt: "2020-06-29T19:15:25.302693Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Collateral Guard Web Service",
                      updatedAt: "2020-06-29T19:15:25.392503Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy CWI TDAF Web",
                      updatedAt: "2020-06-29T19:15:25.483827Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy CWI TDFS Web",
                      updatedAt: "2020-06-29T19:15:25.590912Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy MDA",
                      updatedAt: "2020-06-29T19:15:25.680337Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy RDPRM",
                      updatedAt: "2020-06-29T19:15:25.787786Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Streamloan Database",
                      updatedAt: "2020-06-29T19:15:25.892323Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName:
                        "Registry CD / CD Legacy Streamloan Document Generator",
                      updatedAt: "2020-06-29T19:15:25.997060Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD Legacy Streamloan Web",
                      updatedAt: "2020-06-29T19:15:26.102078Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD LVS SPAES Database",
                      updatedAt: "2020-09-17T15:51:39.768845Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD LVS SPAES Web Service",
                      updatedAt: "2020-09-17T15:52:10.088631Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD QMS Database",
                      updatedAt: "2020-09-17T15:52:26.586832Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD QMS Regsitry Web",
                      updatedAt: "2020-09-17T15:52:36.796445Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Registry CD / CD QMS Universal Web",
                      updatedAt: "2020-09-17T15:52:50.041062Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Corporate IT",
              factsheets: {
                totalCount: 1,
                edges: [
                  {
                    node: {
                      displayName: "LeanIX Cloud",
                      updatedAt: "2020-07-03T18:57:33.835423Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Infrastructure: CSS",
              factsheets: {
                totalCount: 3,
                edges: [
                  {
                    node: {
                      displayName: "Microsoft Office 365 Enterprise",
                      updatedAt: "2020-08-23T16:30:02.607691Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft SCCM",
                      updatedAt: "2020-07-14T19:10:28.387366Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Zoom Cloud",
                      updatedAt: "2020-08-23T16:31:07.368212Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Infrastructure: ADS",
              factsheets: {
                totalCount: 1,
                edges: [
                  {
                    node: {
                      displayName: "Finance Service 10.5",
                      updatedAt: "2021-06-25T01:58:38.017174Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Important",
              factsheets: {
                totalCount: 6,
                edges: [
                  {
                    node: {
                      displayName: "Bradmark Surveillance",
                      updatedAt: "2020-07-08T18:05:32.432648Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Elastic Stack",
                      updatedAt: "2020-07-08T18:22:04.289156Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "IBM DB2",
                      updatedAt: "2020-12-08T15:20:37.059748Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Microsoft SQL Server",
                      updatedAt: "2020-12-11T18:45:55.215555Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Oracle Database and DBMS",
                      updatedAt: "2020-06-17T16:58:40.611608Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "PostgreSQL",
                      updatedAt: "2020-06-17T16:43:46.323542Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Infrastructure: ICS",
              factsheets: {
                totalCount: 3,
                edges: [
                  {
                    node: {
                      displayName: "Cisco Intersight Cloud",
                      updatedAt: "2020-07-28T14:09:45.728745Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Cisco Systems Cloud Security Broker Cloud",
                      updatedAt: "2020-07-03T13:43:15.921221Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "IBM VoiceTrust",
                      updatedAt: "2020-07-07T16:00:19.607381Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Apic",
              factsheets: {
                totalCount: 2,
                edges: [
                  {
                    node: {
                      displayName: "eStrataHub",
                      updatedAt: "2020-11-19T18:20:22.308448Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "TCOL",
                      updatedAt: "2020-11-19T21:30:56.701534Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Infrastructure: DC Admins",
              factsheets: {
                edges: [
                  {
                    node: {
                      displayName: "HCL BigFix",
                      updatedAt: "2020-12-08T15:22:59.753053Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Infrastructure: EMS",
              factsheets: {
                totalCount: 1,
                edges: [
                  {
                    node: {
                      displayName: "Cisco AppDynamics Cloud",
                      updatedAt: "2020-07-03T14:14:25.644962Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Infrastructure: Internetworking",
              factsheets: {
                totalCount: 1,
                edges: [
                  {
                    node: {
                      displayName:
                        "F5 Networks BIG-IP Application Security Manager (ASm)",
                      updatedAt: "2020-06-17T16:06:11.969582Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "Analytics Data Engineering",
              factsheets: {
                totalCount: 2,
                edges: [
                  {
                    node: {
                      displayName: "Cloudera CDP",
                      updatedAt: "2020-12-15T17:03:51.387990Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                  {
                    node: {
                      displayName: "Tableau",
                      updatedAt: "2020-12-15T17:05:33.176722Z",
                      qualitySeal: "BROKEN",
                    },
                  },
                ],
              },
            },
          },
          {
            node: {
              name: "DevOps",
              factsheets: {
                totalCount: 0,
                edges: [],
              },
            },
          },
        ],
      },
    },
    errors: null,
  },
  // array that will hold the transformed response, in form of rows
  rows: [],
  //array to store the row data
  applications: [],
  // array to store the table's columns key and label
  columns: [
    {
      key: "name",
      header: "Tag Group",
    },

    {
      key: "displayName",
      header: "Applications",
    },

    {
      key: "totalCount",
      header: "Total Factsheets",
    },

    {
      key: "completeFactsheets",
      header: "Complete Factsheets",
    },

    {
      key: "completion",
      header: "Completion Percentage",
    },

    {
      key: "lastUpdatedDate",
      header: "Last Updated Date",
    },
  ],

  // variable to hold last updated date
  lastUpdatedDate: "n/a",
};

const methods = {
  async initializeReport() {
    await lx.init();
    await lx.ready({});
  },
  async fetchGraphQLData() {
    //GraphQL query that fetches data and return a JSON object, go to LeanIX->Administration->Tools->GraphQL to write your own queries
    // const query = `
    // {
    //   allFactSheets(filter: {facetFilters: [{facetKey: "FactSheetTypes", operator: OR, keys: ["Application"]}]}) {
    //     totalCount
    //     edges {
    //       node {
    //         displayName
    //         id
    //         type
    //         comments {
    //           edges {
    //             node {
    //               message
    //             }
    //           }
    //         }
    //         tags {
    //           name
    //           id
    //         }
    //         ... on Application {
    //           relApplicationToITComponent {
    //             edges {
    //               node {
    //                 factSheet {
    //                   updatedAt
    //                 }
    //               }
    //             }
    //           }
    //         }
    //       }
    //     }
    //   }
    // }

    // `;
    const query = `{
      allFactSheets(filter: {facetFilters: [{facetKey: "FactSheetTypes", operator: OR, keys: ["Application"]}]}) {
        totalCount
        edges {
          node {
            displayName
            id
            tags {
              tagGroup {
                id
              }
            }
            type
            qualitySeal
            comments {
              edges {
                node {
                  message
                }
              }
            }
            ... on Application {
              subscriptions {
                edges {
                  node {
                    user {
                      displayName
                      role
                    }
                  }
                }
              }
              relApplicationToITComponent {
                totalCount
                edges {
                  node {
                    id
                    factSheet {
                      name
                      displayName
                      type
                      updatedAt
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    `;
    lx.showSpinner();
    try {
      this.mapResponseToRows();
    } finally {
      lx.hideSpinner();
    }
  },

  exportToXLSX(columns, rows) {
    lx.showSpinner();
    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet("Applications");
    worksheet.columns = columns;
    worksheet.addRows(rows);

    return workbook.xlsx

      .writeBuffer()
      .then((buffer) => {
        const blob = new Blob([buffer], {
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        });
        saveAs(blob, "document.xlsx");
        lx.hideSpinner();
      })
      .catch((err) => console.error("error while exporting to excel", err));
  },

  mapResponseToRows() {
    if (this.response === null) return;
    this.rows = this.response.data.allTags.edges.map((edge) => {
      let { name, factsheets } = edge.node;

      // Open FactSheet on a new page

      // Utilizes a common part of the URL, and appends factsheet type and id to get unique links for every FactSheet

      // if (API_TOKEN === "qnY8AWkqYJMdzU6nA7LPE5TcbR2D4g3q6LyyCghW") {
      //   var displayNameApplications =
      //     '<u><a href="https://teranet.leanix.net/TeranetSandbox/factsheet/' +
      //     type +
      //     "/" +
      //     id +
      //     '" style="color:blue" target="_blank">' +
      //     displayName +
      //     "</a><u>";
      // } else {
      //   var displayNameApplications =
      //     '<u><a href="https://teranet.leanix.net/TeranetProduction/factsheet/' +
      //     type +
      //     "/" +
      //     id +
      //     '" style="color:blue" target="_blank">' +
      //     displayName +
      //     "</a><u>";
      // }

      // Display Name of all Application Factsheets under a single Tag Group
      var displayName = factsheets.edges.map((edge) => edge.node.displayName);

      // // Total Number of children Application Factsheets under a Tag Group
      var totalCount = factsheets.totalCount;

      // Completion numbers based on quality Seal
      var qualitySealApplicationFactsheet = factsheets.edges.map(
        (edge) => edge.node.qualitySeal
      );

      // Number of Complete Factsheets
      const completeFactsheets = qualitySealApplicationFactsheet.filter(
        function (element) {
          return element == "APPROVED";
        }
      ).length;

      // Completion Percentage
      var completion =
        Math.floor(
          ((parseFloat(completeFactsheets) * 1.0) / parseFloat(totalCount)) *
            100
        ) + "%";

      if (totalCount == 0) {
        completion = "n/a";
      }

      // Get the Latest Update Date for all the Application Factsheets and display it for each Tag Group

      // Returns an array of ISO 8601 dates
      var lastUpdatedDate = factsheets.edges.map((edge) => edge.node.updatedAt);

      // Sort in a descending Order
      lastUpdatedDate.sort(function (a, b) {
        return a > b ? -1 : a < b ? 1 : 0;
      });

      // Display the ISO 8601 date in a normal form by using a substring of the whole string
      lastUpdatedDate = lastUpdatedDate[0];
      var strMaxDate = String(lastUpdatedDate);
      var date = new Date(strMaxDate);
      var day = date.getDate();
      var year = date.getFullYear();
      var month = date.getMonth() + 1;
      var months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];

      if (totalCount == 0) {
        lastUpdatedDate = "n/a";
      } else {
        lastUpdatedDate = months[month - 1] + " " + day + " " + year;
      }

      // NOTE: To optimize and replace the logic in the code above, one may use the Javascript "reduce" function

      return {
        name,
        displayName,
        totalCount,
        completeFactsheets,
        completion,
        lastUpdatedDate,
      };
    });

    this.computeTableColumns(); // <-- call the computeTableColumns method here!
  },

  computeTableColumns() {
    const columnKeys = [
      "displayName",
      "completion",
      "totalCount",
      "CompleteFactsheets",
      "subscriptions",
      "comments",
      "displayNameITComponents",
      "lastUpdatedDate",
    ];
    this.columns = columnKeys.map((key) => ({
      key,
      label: lx.translateField("Application", key),
    }));
  },
};

window.initializeContext = () => {
  return {
    ...state,
    ...methods,
  };
};
