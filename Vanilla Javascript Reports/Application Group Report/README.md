## Sample Custom Report

The following is a sample report with boilerplate code to build Teranet Custom Reports.

## How to Use

- Open a terminal of your choice and navigate to the directory you'd like to put your project in.
- Clone the repository by running the fllowing command :- `git clone https://gitlab.com/vanshmago/custom-report.git`
- The project files are now on your machine. Refactor the code according to your needs to build custom reports.

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Project setup](#project-setup)
- [Available scripts](#available-scripts)

## Project setup

This project was bootstrapped with [leanix-reporting-cli](https://github.com/leanix/leanix-reporting-cli).

1. `npm install -g @leanix/reporting-cli`
1. `npm install` in project directory
1. create a `lxr.json` file in project directory (please see [Getting started](https://github.com/leanix/leanix-reporting-cli#getting-started))

## Available scripts

In the project directory, one can run:

`npm start`

This command will start the local development server. Please make sure you have properly configured `lxr.json` first.
It will take the specified API Token from `lxr.json` and automatically do a login to the workspace.

`npm run build`

Builds the report and outputs the build result into `dist` folder.


`npm run upload`

Uploads the report to the workspace configured in `lxr.json`.
Please see [Uploading to LeanIX workspace](https://github.com/leanix/leanix-reporting-cli#uploading-to-leanix-workspace).
